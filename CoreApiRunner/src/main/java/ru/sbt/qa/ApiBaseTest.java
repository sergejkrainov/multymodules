package ru.sbt.qa;

import ru.sbt.qa.libs.ApiRequest;
import ru.sbt.qa.stepDefs.AfterTest;

/**
 * Created by out-mashnev-ma on 22.09.2017.
 */
public abstract class ApiBaseTest extends AfterTest {
    private ApiRequest apiRequest = new ApiRequest();

    protected <T> T createTransportProxy(Class<T> requestClass) {
        return apiRequest.createTransportProxy(requestClass);
    }
    public abstract void createRequestModel() throws Exception;
    public abstract void sendRequest() throws Exception;
    public abstract void checkResponse() throws Exception;
}
