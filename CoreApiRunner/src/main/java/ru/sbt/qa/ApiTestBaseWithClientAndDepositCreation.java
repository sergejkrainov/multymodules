package ru.sbt.qa;

import com.sbt.pprb.services.api.pprb.opentransactionpurse.dto.CashlessOpenDepositRs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.info.api.model.ProductDto;
import ru.sbt.qa.libs.ClientUtil;
import ru.sbt.qa.libs.DepositUtil;

/**
 * Created by out-oreshin-aa on 22.03.2018.
 */
public abstract class ApiTestBaseWithClientAndDepositCreation extends ApiBaseTest {
    protected static final Logger LOG = LoggerFactory.getLogger(ApiTestBaseWithClientAndDepositCreation.class);

    protected long partyId;
    protected String productId;
    protected ProductDto product;

    protected ClientUtil clientUtil = new ClientUtil();
    protected DepositUtil depositUtil = new DepositUtil();

    protected ApiTestBaseWithClientAndDepositCreation() throws Exception {
        this.partyId = clientUtil.getExistingClient().getPartyId();
        CashlessOpenDepositRs openDepositRs = depositUtil.createDeposit(partyId);
        productId = openDepositRs.getProductId();
        product = depositUtil.getProductById(productId);
    }
}
