

package ru.sbt.qa.libs;

/**
 * Created by sbt-kraynov-sa on 24.01.2018.
 */
import com.suntotem.ALM.AlmStep;
import com.suntotem.ALM.Api;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

/**
 * Методы логирования в HP ALM
 */

public class AlmLogger {

    private static final Map<Integer, String> stepStatuses = new LinkedHashMap<>();
    private static final Map<Integer, String> stepResults = new LinkedHashMap<>();
    private static final Map<Integer, String> stepScreenShot = new LinkedHashMap<>();
    public static Properties properties =  ResourceUtil.getPropertiesFromFile("../configs/alm.properties");

    // --================= ALM PROPERTY =================-- //
    private static final String ALM_SERVER = properties.getProperty("alm.server") + "/" + "qcbin";
    private static final String ALM_USER = properties.getProperty("alm.user");
    private static final String ALM_PASSWORD = properties.getProperty("alm.password");
    private static final boolean ALM_STEP_UPDATE = Boolean.parseBoolean(properties.getProperty("alm.step.update"));

    // --================= ALM STATUSES =================-- //
    private static final String PASSED = "Passed";
    private static final String FAILED = "Failed";

    private AlmLogger() {
    }



    /**
     * Общий метод формирования и отправки отчета в HP ALM
     *
     * @param scenarioStatus - глобальный статус сценария
     *
     */
    public static void almReport(String scenarioStatus) {
        if (!("0".equals(properties.getProperty("alm.test.set.id"))) && !("".equals(properties.getProperty("alm.test.set.id")))) {
            if (ALM_STEP_UPDATE) {
                sendAlmReport();
            } else {
                sendAlmReport(scenarioStatus);
            }
        }
    }

    /**
     * Метод отправки отчет в HP ALM c обновлением глобального статуса (без артефактов)
     *
     * @param scenarioStatus - глобальный статус сценария
     * @return Boolean - результат отправки отчета
     */
    private static boolean sendAlmReport( String scenarioStatus) {
        String finalStatus;
        finalStatus = "passed".equalsIgnoreCase(scenarioStatus) ? PASSED : FAILED;
        // Отправка статуса в HP ALM
        boolean result = new Api().addResultsToAlm(ALM_SERVER, properties.getProperty("alm.domain"), properties.getProperty("alm.project"),
                ALM_USER, ALM_PASSWORD
                , Integer.parseInt(properties.getProperty("alm.test.set.id")), Integer.parseInt(properties.getProperty("alm.test.id")), finalStatus);
        if (!result) {
            System.out.println("Ошибка при обновлении информации для тест-кейса " + properties.getProperty("alm.test.id") + " в HP ALM!  finalTESTStatus: " + finalStatus);
            return false;
        }
        System.out.println("Информация для тест-кейса " + properties.getProperty("alm.test.id") + " в HP ALM успешно обновлена finalTESTStatus: " + finalStatus);
        return true;
    }

    /**
     * Метод отправки отчет в HP ALM c пошаговым обновлением статуса (с артефактами)
     *
     * @return Boolean - результат отправки отчета
     */
    private static boolean sendAlmReport() {
        LinkedList<AlmStep> linkedList = new LinkedList<>();
        // Отправка статуса в HP ALM
        for (int i = 0; i < stepStatuses.size(); i++) {
            linkedList.add(i, new AlmStep(stepResults.get(i + 1), stepStatuses.get(i + 1), stepScreenShot.get(i + 1)));
        }

        // Отправка информации в ALM
        boolean result = new Api().addResultsToAlm(ALM_SERVER, properties.getProperty("alm.domain"), properties.getProperty("alm.project"), ALM_USER, ALM_PASSWORD
                , Integer.parseInt(properties.getProperty("alm.test.set.id")), Integer.parseInt(properties.getProperty("alm.test.id")), linkedList);
        if (!result) {
            System.out.println("Ошибка при обновлении информации для тест-кейса " + properties.getProperty("alm.test.id") + " в HP ALM!");
            return false;
        }
        System.out.println("Информация для тест-кейса " + properties.getProperty("alm.test.id") + " в HP ALM успешно обновлена");
        return true;
    }
}