package ru.sbt.qa.libs;

/**
 * Created by sbt-kraynov-sa on 13.12.2017.
 */

import com.sbt.access_system.client.API.services.TicketProvider;
import com.sbt.core.amqp.services.TransportProxyFactory;
import com.sbt.core.envelope.container.api.EnvelopeObjectFactory;
import com.sbt.core.envelope.container.api.EnvelopeObjectFactoryBinder;
import com.sbt.core.transport.dispatcher.MessageDispatcherInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;


public class ApiRequest {
    private static final Logger LOG = LoggerFactory.getLogger(ApiRequest.class);
    private final EnvelopeObjectFactory envelopeObjectFactory = (new EnvelopeObjectFactoryBinder()).getInstance();
    private static MessageDispatcherInitializer md;

    public ApiRequest() {
        this(getCurrentNodeId());
    }

    private static String getCurrentNodeId(){
        String computerName = null;
        try {
            String host;
            try {
                host = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                throw new RuntimeException(e);
            }
            System.out.println("host = " + host);
            String userDir = System.getProperty("user.dir");
            String[] userDirSplitted = userDir.split(userDir.contains("\\") ? "\\\\" : "/");
            String projectName = userDirSplitted[userDirSplitted.length - 1];
            System.out.println("project name = " + projectName);
            computerName = "AFT--" + host + "--" + projectName;
        } catch (Exception e) {
            LOG.warn("Не смогли получить имя компьютера! " + e.getMessage());
        }
        if (Objects.equals(computerName, null)){
            return "TEST";
        } else {
            return computerName;
        }
    }

    public ApiRequest(String nodeId) {
        ApiUtils.setSystemProperties(nodeId);
        md = initMessageDispatcher();
    }

    public TransportProxyFactory createTransportProxyFactory(boolean fakeTicket) {
        Object ticketProvider;
        if(fakeTicket) {
            ticketProvider = new FakeTicketProvider();
        } else {
            CurrentTicketProvider currentTicketProvider = new CurrentTicketProvider();
            currentTicketProvider.setEnvelopeObjectFactory(this.envelopeObjectFactory);
            ticketProvider = currentTicketProvider;
        }

        FakeUserProvider userProvider = new FakeUserProvider();
        return this.envelopeObjectFactory.createExternalTransportProxyFactory((TicketProvider)ticketProvider, userProvider);
    }

    public <T> T createTransportProxy(Class<T> nameClass, boolean fakeTicket) {
        TransportProxyFactory transport = this.createTransportProxyFactory(fakeTicket);
        return transport.createTransportProxy(nameClass);
    }

    public Object createTransportProxy(String className, boolean fakeTicket) {
        Class c = null;

        try {
            c = Class.forName(className);
        } catch (ClassNotFoundException var5) {
            LOG.error("Class not found with name: " + className, var5);
        }

        return c != null?this.createTransportProxy(c, fakeTicket):null;
    }

    public static synchronized MessageDispatcherInitializer initMessageDispatcher() {
        if(null == md) {
            md = new MessageDispatcherInitializer();

            try {
                md.initialize();
            } catch (Exception var1) {
                throw new RuntimeException(var1);
            }

            return md;
        } else {
            return md;
        }
    }

    public <T> T createTransportProxy(Class<T> requestClass) {
        LOG.info("Создание прокси транспорта для \"" + requestClass.getSimpleName() + "\"");

        try {
            boolean FAKE_TICKET_PROVIDER = true;
            return createTransportProxy(requestClass, FAKE_TICKET_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error("Не создался прокси транспорта. Вероятно, Кафка мертв\n" + e.getMessage());
        }
    }
}