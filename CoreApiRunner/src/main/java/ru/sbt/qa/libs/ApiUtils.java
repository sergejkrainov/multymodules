package ru.sbt.qa.libs;

/**
 * Created by sbt-kraynov-sa on 13.12.2017.
 */
public class ApiUtils {
    private ApiUtils() {
    }

    public static void setSystemProperties(String nodeId) {
        System.setProperty("node.id", nodeId);
        System.setProperty("config-store.disabled", "true");
        System.setProperty("jndi-store.disabled", "true");
    }
}