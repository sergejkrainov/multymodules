package ru.sbt.qa.libs;

import com.google.common.collect.ImmutableMap;
import com.sbt.bm.ucp.api.*;
import com.sbt.bm.ucp.api.projections.Projected;
import com.sbt.bm.ucp.api.projections.Projection;
import com.sbt.bm.ucp.common.model.dictionary.ContactStatus;
import com.sbt.bm.ucp.common.model.dictionary.ContactUsageType;
import com.sbt.bm.ucp.common.model.dictionary.ElectronicAddressSubType;
import com.sbt.bm.ucp.common.model.dictionary.PartyType;
import com.sbt.bm.ucp.common.model.party.contact.ElectronicAddress;
import com.sbt.bm.ucp.common.model.party.identificatation.AbstractIdentification;
import com.sbt.bm.ucp.retail.model.individual.Individual;
import com.sbt.bm.ucp.retail.xcommon.service.impl.UcpModelsConverterService;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import ru.sbt.qa.calVars.CalculatedVariables;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ClientUtil {
    private static final Logger LOG = LoggerFactory.getLogger(ClientUtil.class);
    private static final long CALL_TIMEOUT = 120L;

    /**
     * Путь до шаблона запроса на создания клиента
     */
    private static final String XML_TEMPLATE_FOR_CLIENT_CREATION = System.getProperty("user.dir") + "/../CoreApiRunner/src/main/resources/data/defaults/templates/createClient.xml";

    /**
     * Инициализация используемых API
     */
    private ApiRequest apiRequest = new ApiRequest();
    private ClientApiServiceRequest clientApiServiceRequest = apiRequest.createTransportProxy(ClientApiServiceRequest.class);
    private SearchApiServiceRequest searchApiServiceRequest = apiRequest.createTransportProxy(SearchApiServiceRequest.class);

    private Individual individual;
    private ElectronicAddressSubType emailSubType;
    private ContactUsageType emailUsageType;
    private PartyType partyType;
    private ContactStatus contactStatus;
    private String generatedEmail;
    private ElectronicAddress email;
    private List<AbstractIdentification> identifications;
    private AbstractIdentification docs;
    private String docNumber;
    private String docSeries;
    private ApiResult<Individual> result;

    private void createNewClient(String xmlPath) throws Exception {
        LOG.info("Генерирование данных для создания клиента");
        
        individual = getClientTemplateFromXml(xmlPath);
        emailSubType = new ElectronicAddressSubType();
        emailSubType.setCode(1);
        emailUsageType = new ContactUsageType();
        emailUsageType.setCode(11);
        partyType = new PartyType();
        partyType.setId(1L);
        emailUsageType.setPartyType(partyType);
        contactStatus = new ContactStatus();
        contactStatus.setCode(1);
        generatedEmail = System.currentTimeMillis() + "@aft.ru";
        email = ElectronicAddress.builder().setUsageType(emailUsageType)
                .setEmailSubType(emailSubType)
                .setContactStatus(contactStatus)
                .setEmail(System.currentTimeMillis() + "@aft.ru")
                .build();
        individual.setElectronicAddresses(Arrays.asList(email));
        identifications = new ArrayList<>();
        docs = individual.getIdentifications().get(0);
        docNumber = NumberUtil.getRandomNumericLetters(6);
        docSeries = NumberUtil.getRandomNumericLetters(2) + " " + NumberUtil.getRandomNumericLetters(2);
        docs.setDocumentNumber(docNumber);
        docs.setDocumentSeries(docSeries);
        identifications.add(docs);
        individual.setIdentifications(identifications);
        
        if (CalculatedVariables.flagCOD) {
            String externalId = System.getProperty("tb") + "-" + NumberUtil.getRandomNumericLetters(9) + "-" + NumberUtil.getRandomNumericLetters(6);
            individual.getEquivalents().get(0).setExternalSystemClientId(externalId);
        } else {
            individual.getEquivalents().get(0).setExternalSystemClientId(NumberUtil.getRandomNumericLetters(9));
        }
        
        System.setProperty("email", email.getEmail());
        
        LOG.info("Сгенерирован docNumber \"" + docNumber + "\"");
        LOG.info("Сгенерирован docSeries \"" + docSeries + "\"");
        LOG.info("Сгенерирован generatedEmail \"" + generatedEmail + "\"");
        LOG.info("Сгенерирован ExternalSystemClientId \"" + individual.getEquivalents().get(0).getExternalSystemClientId() + "\"");

        createIndividRequest();
    }

    /**
     * Отправка запроса на создание клиента
     */
    private void createIndividRequest() {
        result = null;

        LOG.info("Создание клиента");
        LOG.info("Запрос на создание клиента");
        LOG.info(individual.toString());

        result = clientApiServiceRequest.create(individual).withTimeout(CALL_TIMEOUT, TimeUnit.SECONDS).execute();

        LOG.info("Ответ запроса на создание клиента");
        LOG.info(ToStringBuilder.reflectionToString(result));

        Assert.assertEquals("Не удалось создать клиента", ApiResultStatus.SUCCESS, result.getStatus());

 /*       Assertions.assertAll("Создание клиента не удалось",
                () -> Assert.assertEquals("result of execute create client NOT SUCCESS", ApiResultStatus.SUCCESS, result.getStatus()),
                () -> Assertions.assertTrue(result.getErrors().isEmpty(),
                        () -> result.getErrors().stream()
                                .map(err -> String.format("\tКод ошибки: %s, Текст ошибки: %s", err.getCode(), err.getMessage()))
                                .collect(Collectors.joining("\n", "Получены ошибки:\n", ""))
                ),
                () -> {
                    Assert.assertNotNull("Результат пустой", result.getResult());
                    Assert.assertEquals("Не пройдена проверка уникальности по email в ответе апишки",
                            System.getProperty("email"), result.getResult().getElectronicAddresses().get(0).getEmail());
                }
        );*/

        LOG.info("Параметры созданного клиента \"" + result.getResult() + "\"");
    }

    public Projected<Individual> getExistingClient(String xmlPath) throws Exception {
        createNewClient(xmlPath);

        LOG.info("Проверка создания клиента (поиск по электронной почте)");

        SearchSpecification searchPropertyMap = SearchSpecification.builder()
                .withEmail(System.getProperty("email"))
                .build();

        SearchParameters searchParams = new SearchParameters(UcpSearchStrategy.EMAIL,
                searchPropertyMap).includeUnreliable(true);

        ApiResult<List<Projected<Individual>>> result = searchApiServiceRequest.search(searchParams, getProjection()).withTimeout(CALL_TIMEOUT, TimeUnit.SECONDS).execute();

        Assert.assertEquals("API поиска выдало ошибку", ApiResultStatus.SUCCESS, result.getStatus());
        Assert.assertTrue("В ответе присутсвуют ошибки", result.getErrors().isEmpty());
        Assert.assertFalse("В ответе отсутсвуют результаты (ничего не найдено)", result.getResult().isEmpty());

        return result.getResult().get(0);
    }

    public Projected<Individual> getExistingClient() throws Exception {
        return getExistingClient(XML_TEMPLATE_FOR_CLIENT_CREATION);
    }

    public Individual getClientTemplateFromXml(String xmlPath) throws Exception {
        LOG.info("Чтение шаблона из xml");

        String xml = FileLib.readFromFile(xmlPath, "UTF-8");
        Jaxb2Marshaller jaxb2Marshaller = setupMarshaller(Individual.class);
        Individual individual;
        individual = new UcpModelsConverterService(jaxb2Marshaller).convertRequestXmlToIndividual(xml);
        return individual;
    }

    private Jaxb2Marshaller setupMarshaller(Class clazz) throws Exception {
        LOG.info("Запуск маршаллера");

        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan(new String[]{"com.sbt.bm"});
        jaxb2Marshaller.setMarshallerProperties(ImmutableMap.of("jaxb.fragment", true));
        jaxb2Marshaller.setMappedClass(clazz);
        jaxb2Marshaller.afterPropertiesSet();
        return jaxb2Marshaller;
    }

    private Projection<Individual> getProjection() {
        Set<UcpProjectionProperty> ucpProjectionProperties = new HashSet<>();
        ucpProjectionProperties.addAll(Arrays.asList(UcpProjectionProperty.IDENTIFICATIONS, UcpProjectionProperty.NAMES, UcpProjectionProperty.BIRTH_DATE));
        return Projection.project(ucpProjectionProperties);
    }

    private Projection<Individual> getSearchProjection() {
        Set<UcpProjectionProperty> ucpProjectionProperties = new HashSet<>();
        ucpProjectionProperties.addAll(Arrays.asList(UcpProjectionProperty.IDENTIFICATIONS,
                UcpProjectionProperty.NAMES, UcpProjectionProperty.BIRTH_DATE, UcpProjectionProperty.PARTY_TO_PARTY_GROUPS,
                    UcpProjectionProperty.MANAGER_EMPLOYEE_INFO));
        return Projection.project(ucpProjectionProperties);
    }

    /**
     * Проверка на существование в гриде пользователся с переданным clientId
     * @param clientId - 19 значный номер клиента
     * @return истина, если нашелся клиент
     */
    public boolean checkClientExistence(String clientId) {
        LOG.info("Проверка существования клиента по clientId");

        GetByIdRequestParameters getByIdRequestParameters = new GetByIdRequestParameters(Long.parseLong(clientId));
        ApiResult<UcpSearchResult> response = clientApiServiceRequest.get(getByIdRequestParameters)
                .withTimeout(60, TimeUnit.SECONDS)
                .execute();
        UcpSearchResult searchResult = response.getResult();

        if (searchResult != null) {
            Individual client = searchResult.getIndividual();
            if (client == null) {
                LOG.info("Нет пользователя с partyId \"" + clientId + "\"");
                return false;
            } else {
                LOG.info("Есть пользователь с partyId \"" + clientId + "\"");
                return true;
            }
        }
        return false;
    }

    public ApiResult<List<Projected<Individual>>> getClientById(Long id) {
        GetByIdsParameters getByIdsParameters = new GetByIdsParameters(id);
        return searchApiServiceRequest
                .getByIds(getByIdsParameters, getSearchProjection())
                .withTimeout(CALL_TIMEOUT, TimeUnit.SECONDS).execute();
    }

    /**
     * Вначале определяем SearchSpecification, где указываем, по какому параметру искать. Далее, кладем в SearchParameters.
     * Например:  SearchSpecification searchSpecification = SearchSpecification.builder().withExternalClientId("38-333788550-120909").withExternalSystemCode(Long.parseLong(System.getProperty("tb"))).build();
     * SearchParameters searchParameters = new SearchParameters(UcpSearchStrategy.EXTERNAL_ID, searchSpecification).includeUnreliable(true);
     * Который и передаем в данный метод
     * @param searchParams
     * @return
     * @throws Exception
     */
    public List<Projected<Individual>> searchIndividualByParam(SearchParameters searchParams) throws Exception {
        ApiResult<List<Projected<Individual>>> result =
                searchApiServiceRequest.search(searchParams, getProjection()).withTimeout(CALL_TIMEOUT, TimeUnit.SECONDS).execute();
        if (!result.getStatus().equals(ApiResultStatus.SUCCESS)) {
            throw new Exception("Ошибка при поиске клиента:" + "Code: " + result.getErrors().get(0).getCode() + "Message: " + result.getErrors().get(0).getMessage());
        }
        return result.getResult();
    }
}
