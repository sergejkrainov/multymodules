package ru.sbt.qa.libs;

import com.sbt.access_system.client.API.model.Ticket;
import com.sbt.access_system.client.API.services.TicketProvider;
import com.sbt.core.envelope.container.api.EnvelopeObjectFactory;

/**
 * Created by sbt-kraynov-sa on 13.12.2017.
 */
public class CurrentTicketProvider implements TicketProvider {
    private static final String USER = "ukl_ht_user";
    private static final String PASSWORD = "q1w2e3r4";
    private static final String IP = "10.116.146.200";
    private EnvelopeObjectFactory envelopeObjectFactory;

    public CurrentTicketProvider() {
    }

    public Ticket getTicket() {
        return this.envelopeObjectFactory.createExternalSystemSecurityService().login("ukl_ht_user", "q1w2e3r4", "10.116.146.200");
    }

    public void setEnvelopeObjectFactory(EnvelopeObjectFactory envelopeObjectFactory) {
        this.envelopeObjectFactory = envelopeObjectFactory;
    }

    public EnvelopeObjectFactory getEnvelopeObjectFactory() {
        return this.envelopeObjectFactory;
    }
}
