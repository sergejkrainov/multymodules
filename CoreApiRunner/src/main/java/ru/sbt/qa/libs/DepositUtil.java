package ru.sbt.qa.libs;

import com.sbt.pprb.services.api.pprb.opentransactionpurse.SrvInitiateOpenTransactionPurseRequest;
import com.sbt.pprb.services.api.pprb.opentransactionpurse.dto.CashlessOpenDepositRq;
import com.sbt.pprb.services.api.pprb.opentransactionpurse.dto.CashlessOpenDepositRs;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.util.ResourceUtils;
import ru.sbrf.depositpf.info.api.DepositInformationServiceRequest;
import ru.sbrf.depositpf.info.api.Result;
import ru.sbrf.depositpf.info.api.mask.ProductSearchMask;
import ru.sbrf.depositpf.info.api.model.ProductDto;
import ru.sbt.qa.calVars.CalculatedVariables;

import java.io.FileNotFoundException;
import java.io.IOException;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by out-oreshin-aa on 21.03.2018.
 */
public class DepositUtil {
    private static final String TEMPLATE_DIR = "templates";
    private static SysOutUtil out = new SysOutUtil();
    private static final Logger LOG = Logger.getLogger(DepositUtil.class);
    private static final String JSON_PARSE_ERROR = "Не удалось десериализовать JSON-файл: '%s' в класс:'%s'%n Ошибка:%s";

    /**
     * Инициализация используемых API
     */
    private ApiRequest apiRequest = new ApiRequest();
    private DepositInformationServiceRequest depositInformationServiceRequest = apiRequest.createTransportProxy(DepositInformationServiceRequest.class);
    private SrvInitiateOpenTransactionPurseRequest transactionPurseRequest = apiRequest.createTransportProxy(SrvInitiateOpenTransactionPurseRequest.class);

    public DepositInformationServiceRequest getDepositInformationService() {
        return depositInformationServiceRequest;
    }

    public SrvInitiateOpenTransactionPurseRequest getTransactionPurseRequest() {
        return transactionPurseRequest;
    }

    public CashlessOpenDepositRs createDeposit(long partyID) {
        LOG.info("Создание депозита");

        CashlessOpenDepositRq req = getJsonData("TransactionPurseRequest", CashlessOpenDepositRq.class);

        req.getClient().setPartyId(String.valueOf(partyID));
        req.setOperationDay(DateUtils.toCalendar(CalculatedVariables.operDay));
        out.printJ(req);

        LOG.info("Выполнение запроса \"SrvInitiateOpenTransactionPurseRequest\"");

        CashlessOpenDepositRs res = getTransactionPurseRequest().invoke(req).execute();

        LOG.info("Проверка ответа \"SrvInitiateOpenTransactionPurseRequest\"");

        if (res.getStatus().getStatusCode() != 0) {
            throw new MMTException(res.getStatus().getStatusCode(), "Ошибка вызова сервиса SrvInitiateOpenTransactionPurse.invoke: " + res.getStatus().getStatusDesc());
        }

        assertNotNull(res.getProductId(), "В ответе заявки открытия счета не найден идентификатор продукта");
        return res;
    }

    private <T> T getJsonData(String fileName, Class<T> clazz) {
        String fullPath = convertPath(fileName + ".json");
        try {
            return JSONLib.parseJsonEx(fullPath,
                    clazz);
        } catch (IOException e) {
            String message = String.format(JSON_PARSE_ERROR, fullPath, clazz.getName(), e.getMessage());
            throw new RuntimeException(message, e);
        }
    }

    protected String convertPath(String fileName) {
        String standName = System.getProperty("standName");

        try {
            return ResourceUtils.getFile("classpath:data/" + standName + '/' + TEMPLATE_DIR + '/' + fileName).getAbsolutePath();
        } catch (FileNotFoundException ignored) {}

        try {
            return ResourceUtils.getFile("classpath:data/defaults/" + TEMPLATE_DIR + '/' + fileName).getAbsolutePath();
        } catch (FileNotFoundException ignored) {}

        return "src/main/resources/data/defaults/" + TEMPLATE_DIR + '/' + fileName;
    }

    public ProductDto getProductById(String id) {
        LOG.info("Получение продукта по id");

        Result<ProductDto> res = getDepositInformationService().getProductById(id, new ProductSearchMask().withContract()).execute();
        if (res.getResultCode() != 0)
            throw new MMTException(res.getResultCode(), "Ошибка вызова сервиса DepositInformationService.getProductById: " + res.getErrorMessage());
        return res.getResult();
    }
}
