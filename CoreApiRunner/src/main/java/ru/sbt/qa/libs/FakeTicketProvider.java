package ru.sbt.qa.libs;

import com.sbt.access_system.client.API.model.Ticket;
import com.sbt.access_system.client.API.services.TicketProvider;

public class FakeTicketProvider implements TicketProvider {
    public FakeTicketProvider() {
    }

    public Ticket getTicket() {
        return new Ticket("fake-ticket");
    }
}
