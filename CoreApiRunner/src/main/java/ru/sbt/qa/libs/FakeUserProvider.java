package ru.sbt.qa.libs;

import com.sbt.access_system.client.API.model.User;
import com.sbt.access_system.client.API.services.UserProvider;

public class FakeUserProvider implements UserProvider {
    public FakeUserProvider() {
    }

    public User getUser() {
        return new User("FakeUser");
    }
}
