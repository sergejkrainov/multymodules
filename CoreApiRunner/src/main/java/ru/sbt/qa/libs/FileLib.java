package ru.sbt.qa.libs;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

public class FileLib {

    private static final Logger LOG = LoggerFactory.getLogger(FileLib.class);
    private static final String ERROR_READ = "Ошибка выполнения теста. Не удалось прочитать данные из файла";
    private static final String ERROR_XML = "Ошибка выполнения теста. Не удалось загрузить шаблон XML";
    private static final String ERROR_WRITE = "Ошибка выполнения теста. Не удалось записать данные в файл";

    /**
     * Функция записи данных в файл
     *
     * @param fName   - путь к файлу
     * @param data    - данные, которые необходимо записать в файл
     * @param charset - кодировка
     */
    public static void writeToFile(String fName, String data, String charset) throws Exception {
        try {
            System.gc();
            File f = new File(fName);
            if (f.exists()) {//удаляем файл, если он уже существует
                f.delete();
            }
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(fName, true), charset));
            BufferedWriter BWf = new BufferedWriter(pw);
            BWf.write(data); // дозаписываем строку в конец файла
            BWf.close(); // не забудем закрыть файл
            pw.close();
            LOG.info("Запись в файл " + fName + " прошла успешно");
        } catch (Exception e) {
            LOG.error(ERROR_WRITE, new Exception(e));
            Assert.fail("ERROR_WRITE");
        }
    }

    /**
     * Функция чтения данных из файла
     *
     * @param fileName - путь к файлу
     * @param charset  - кодировка
     */
    public static String readFromFile(String fileName, String charset) throws Exception {
        String fileContent = "";
        try {
            FileInputStream stream = new FileInputStream(new File(fileName));
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bufferBytes = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            fileContent = Charset.forName(charset).decode(bufferBytes).toString();
            stream.close();
        } catch (Exception e) {
            LOG.error(ERROR_READ, new Exception(e));
            Assert.fail(ERROR_READ);
        }
        return fileContent;
    }
}