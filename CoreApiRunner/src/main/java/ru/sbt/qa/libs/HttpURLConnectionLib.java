package ru.sbt.qa.libs;

import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by sbt-bratuhin-myu on 26.07.2017.
 */
public class HttpURLConnectionLib {

    private static final String USER_AGENT = "Mozilla/5.0";

    // HTTP GET request
    public static HttpMessageResponse sendGet(URL url, String params) throws Exception {


        StringBuilder stringBuilder = null;
        try {
            stringBuilder = new StringBuilder(url.toString());
            stringBuilder.append(params);

            System.out.println("Вызываем сервис по URL: " + allure_attach(stringBuilder.toString()));

            URL currentURL = new URL(stringBuilder.toString());

            HttpURLConnection con = (HttpURLConnection) currentURL.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Charset", "UTF-8");

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
            String line;
            StringBuffer response = new StringBuffer();

            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            in.close();

            return new HttpMessageResponse(currentURL, con.getResponseCode(), con.getResponseMessage(),response.toString());
        } catch (Exception e) {
            System.err.println("\nError: " + e.getMessage());
            throw new Exception(e);
        }
    }

    @Attachment(value = "Get URL", type = "text/plain")
    public static String allure_attach(String args){
        return args;
    }

    public static class HttpMessageResponse{
        public final URL URL;
        public final Integer ResponseCode;
        public final String ResponseMessage;
        public final String ResponseBody;

        public HttpMessageResponse(URL Url, Integer respnceCode, String responseMessage, String responseBody) {
            URL = Url;
            ResponseCode = respnceCode;
            ResponseMessage = responseMessage;
            ResponseBody = responseBody;
        }
    }
}
