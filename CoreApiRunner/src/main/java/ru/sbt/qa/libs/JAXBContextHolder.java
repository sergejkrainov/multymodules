package ru.sbt.qa.libs;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Класс для создания и хранения {@link JAXBContext}
 */
public class JAXBContextHolder {

    private JAXBContextHolder() {
    }

    private static Map<Object, JAXBContext> contextHolder = new ConcurrentHashMap<>();

    public static JAXBContext getContext(Class classForJaxb) {
        return getContextByKey(classForJaxb);
    }

    public static JAXBContext getContext(String packageForJaxb) {
        return getContextByKey(packageForJaxb);
    }

    private static JAXBContext getContextByKey(Class classForJaxb) {
        return getContextByKey(classForJaxb.getPackage().getName());
    }

    private static JAXBContext getContextByKey(Object key) {
        JAXBContext jaxbCtx = contextHolder.get(key);
        if (!contextHolder.containsKey(key)) {
            try {
                if (key instanceof String) {
                    jaxbCtx = JAXBContext.newInstance(key.toString());
                } else {
                    jaxbCtx = JAXBContext.newInstance((Class) key);
                }
                contextHolder.put(key, jaxbCtx);
            } catch (JAXBException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return jaxbCtx;
    }
}
