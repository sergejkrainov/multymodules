package ru.sbt.qa.libs;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;

import java.io.*;

/**
 * Created by sbt-kraynov-sa on 19.12.2017.
 */
public class JSONLib {

    private JSONLib(){}

    public static <T> T parseJson(String jsonFullName, Class<T> clazz) {
        try (Reader reader = new InputStreamReader(new FileInputStream(jsonFullName), "UTF-8")){
            Gson gson = new Gson();
            return gson.fromJson(reader, clazz);
        } catch (Exception e) {
            System.err.println("Error parsing json " + jsonFullName);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Метод десереализации JSON. выбрасывающий исключение, что бы не проверять NPE
     * @param jsonFullName - полный путь до файла, который необходимо сериализовать.
     * @param clazz - тип-класс <T>, в который сериализовать
     * @param <T> - десереализируемйы тип (clazz)
     * @return - десереализированый обьект соотносящийся с типом <T>
     * @throws FileNotFoundException в случае если jsonFullName неверен;
     * @throws UnsupportedEncodingException в том случае, если кодировка не UTF-8.
     * @throws JsonIOException в случаях, когда возникают пробемы с чтением с файла
     * @throws com.google.gson.JsonSyntaxException в случаях,когда в файле нарушен синтаксис JSON.
     */
    public static <T> T parseJsonEx(String jsonFullName, Class<T> clazz) throws IOException {
        try (Reader reader = new InputStreamReader(new FileInputStream(jsonFullName), "UTF-8")){
            Gson gson = new Gson();
            return gson.fromJson(reader, clazz);
        }
    }

}
