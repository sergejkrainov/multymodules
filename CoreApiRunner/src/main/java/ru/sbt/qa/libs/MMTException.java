package ru.sbt.qa.libs;

/**
 * Created by SBT-Kraynov-SA on 20.10.2017.
 */
public class MMTException extends RuntimeException {
    public static final long ERROR_CODE_DEF = -1L;

    private long errorCode;

    public MMTException(String message) {
        this(ERROR_CODE_DEF, message);
    }

    public MMTException(long errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public MMTException(String message, Throwable cause) {
        this(ERROR_CODE_DEF, message, cause);
    }

    public MMTException(long errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public MMTException(Throwable cause) {
        this(ERROR_CODE_DEF, cause);
    }

    public MMTException(long errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    protected MMTException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        this(ERROR_CODE_DEF, message, cause, enableSuppression, writableStackTrace);
    }

    protected MMTException(long errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public long getErrorCode() {
        return errorCode;
    }
}
