package ru.sbt.qa.libs;

/**
 * Created by SBT-Kraynov-SA on 19.10.2017.
 */
class MarshalException extends RuntimeException {

    MarshalException(String message) {
        super(message);
    }

    MarshalException(Throwable cause) {
        super(cause);
    }
}

