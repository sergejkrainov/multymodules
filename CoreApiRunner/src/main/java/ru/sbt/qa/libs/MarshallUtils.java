package ru.sbt.qa.libs;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

public class MarshallUtils {

    private static final String UTF_8 = "UTF-8";

    private MarshallUtils() {
    }

    public static <T> String marshal(T obj) {
        Class clazz = getRealClass(obj);
        JAXBContext jc = getJaxbContext(clazz);

        try {
            StringWriter stringWriter = new StringWriter();

            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, UTF_8);
            marshaller.marshal(obj, stringWriter);

            return stringWriter.toString();
        } catch (JAXBException e) {
            System.err.println("Ups!!!");
            throw new MarshalException(e);
        }
    }

    public static <T> T unmarshal(Class<T> contextClass, String data) {
        if (data == null) {
            throw new MarshalException("Отсутствуют данные для разбора");
        }

        JAXBContext jc = getJaxbContext(contextClass);

        StringReader reader = new StringReader(data);
        try {
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            if (contextClass == null) {
                Object resObj = unmarshaller.unmarshal(new StreamSource(reader));
                if (resObj instanceof JAXBElement) {
                    return ((JAXBElement<T>) resObj).getValue();
                } else {
                    return (T) resObj;
                }
            } else {
                return unmarshaller.unmarshal(new StreamSource(reader), contextClass).getValue();
            }
        } catch (JAXBException e) {
            throw new MarshalException(e);
        }
    }

    public static <T> JAXBElement<T> createRootElement(T value) {
        ClassLoader curCl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(MarshallUtils.class.getClassLoader());

        QName qname = new QName("", value.getClass().getSimpleName());
        Class valueClass = value.getClass();
        JAXBElement<T> result = new JAXBElement<T>(qname, valueClass, null, value);

        Thread.currentThread().setContextClassLoader(curCl);

        return result;
    }

    private static <T> JAXBContext getJaxbContext(Class<T> contextClass) {
        // Делаем JAXBContext не classLoader'ом Оркестровщика.
        ClassLoader curCl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(MarshallUtils.class.getClassLoader());

        JAXBContext jc = JAXBContextHolder.getContext(contextClass);

        Thread.currentThread().setContextClassLoader(curCl);
        return jc;
    }

    private static <T> Class getRealClass(T obj) {
        Class clazz;
        if (obj instanceof JAXBElement) {
            clazz = ((JAXBElement) obj).getValue().getClass();
        } else {
            clazz = obj.getClass();
        }
        return clazz;
    }
}