package ru.sbt.qa.libs;

import java.util.UUID;

/**
 * Вспомогательный функционал для работы с сообзениями
 */
public class MessageUtil {
    public static <T> T unmarshalFromResourceFile(Class<T> clazz, String resourceFileName) {
        String textMsg = ResourceUtil.getTextFromResourceFile(resourceFileName);
        return MarshallUtils.unmarshal(clazz, textMsg);
    }

    public static String getUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
