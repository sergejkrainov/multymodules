package ru.sbt.qa.libs;

import java.util.Random;

/**
 * Created by SBT-Kraynov-SA on 23.11.2017.
 */
public class NumberUtil {

    /**
     * Функция генерации рандомной строки из цифр определенной длины
     * @param countLetters длина строки
     * @return строка определенной длины
     * @throws Exception
     */
    public static String getRandomNumericLetters(int countLetters) throws Exception {

        String letters[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

        Random rnd = new Random();

        String lettersStr = "";

        for(int i = 0; i < countLetters; i++){
            lettersStr+= letters[rnd.nextInt(letters.length)];
        }

        return lettersStr;

    }
}
