package ru.sbt.qa.libs;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Properties;

/**
 * Created by SBT-Serebryanyy-DM on 31.05.2017.
 */
public class ResourceUtil {
    private static final String CHARSET_DEF = "UTF-8";

    /**
     * Считать текстовые данные из файла
     *
     * @param resourceFileName
     * @return
     */
    public static String getTextFromResourceFile(String resourceFileName) {
        try (InputStream inStream = MessageUtil.class.getResourceAsStream("/" + resourceFileName); StringWriter writer = new StringWriter()) {
            IOUtils.copy(inStream, writer, Charset.forName(CHARSET_DEF));
            return writer.toString();
        } catch (Exception e) {
            String msg = "Can't read text data from resource: " + resourceFileName;

            System.out.println(msg + "\r\nError:\r\n" + e.toString());
            throw new RuntimeException(msg, e);
        }
    }

    /**
     * Считать настройки из файла
     *
     * @param resourceFileName
     * @return
     */
    public static Properties getPropertiesFromResourceFile(String resourceFileName) {
        Properties properties = new Properties();
        try (InputStream inStream = MessageUtil.class.getResourceAsStream("/" + resourceFileName)) {
            properties.load(inStream);
            return properties;
        } catch (Exception e) {
            String msg = "Can't read Properties from resource: " + resourceFileName;

            System.out.println(msg + "\r\nError:\r\n" + e.toString());
            throw new RuntimeException(msg, e);
        }
    }

    /**
     * Считать настройки из файла в определенной кодировке
     *
     * @param path
     * @param charset
     * @return
     */
    public static Properties getPropertiesFromFile(String path, String charset) throws Exception{
        FileInputStream input = new FileInputStream(new File(path));
        Properties properties = new Properties();
        properties.load(new InputStreamReader(input, Charset.forName(charset)));
        return properties;
    }

    public static Properties getPropertiesFromFile(String path) {
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream(path)) {
            properties.load(inputStream);
            return properties;
        } catch (Exception e) {
            return new Properties();
        }
    }

    /**
     * Сохранить настройки в файл
     * */
    public static void savePropertiesToResourseFile(Properties properties, String resourceFileName) throws IOException {
        File propFile = new File(resourceFileName);
        if (!propFile.exists()) {
            propFile.createNewFile();
        }
        try (FileOutputStream outputStream = new FileOutputStream(resourceFileName)) {
            properties.store(outputStream, null);
        }
    }

}
