package ru.sbt.qa.libs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.MultilineRecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.text.MessageFormat;

/**
 * Created by sbt-grischenko-oe on 01.12.2016.
 */
public class SysOutUtil {

    private static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    public void printObj(String name, Object object) {
        System.out.println("-----------------------------------------------");
        printJ(object);
        System.out.println("-----------------------------------------------");
    }

    public void printXml(String name, Object object) {
        System.out.println("-----------------------------------------------");
        System.out.println(name + ":\r\n" + serializeXml(object));
        System.out.println("-----------------------------------------------");
    }

    public void printObj(Object object) {
        if (object == null) {
            System.out.println("Null");
        } else {
            printObj(object.getClass().getSimpleName(), object);
        }
    }

    public void printXml(Object object) {
        printXml(object.getClass().getSimpleName(), object);
    }

    public static String serializeXml(Object object) {
        try {
            return MarshallUtils.marshal(object);
        } catch (Exception e) {
            System.err.println("Serialization Error:\r\n" + e.toString());

        }
        return ReflectionToStringBuilder.toString(object, new MultilineRecursiveToStringStyle());
    }

    public SysOutUtil printJ(Object... objs) {
        return printJ(null, objs);
    }

    /**
     * Печатает объекты в JSON-формате.
     *
     * @param format Строка формата, позиции подстановки обозначаются "{}"
     * @param objs   Набор выводимых объектов.
     * @return Self-for-chaining.
     */
    public SysOutUtil printJ(String format, Object... objs) {
        String f = StringUtils.isBlank(format) ? "{}" : format;
        for (int i = 0; i < objs.length; i++) {
            f = f.replaceFirst("\\{}", "{" + i + "}");
        }

        for (Object o : objs) {
            String s = gson.toJson(o);
            System.out.println(MessageFormat.format(f, s));
        }
        return this;
    }

}
