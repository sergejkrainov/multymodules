package ru.sbt.qa.libs;

import org.junit.rules.ErrorCollector;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Класс для получения доступа к списку ошибок из приватного поля родительского класса ErrorCollector
 * Created by out-mashnev-ma on 17.04.2017.
 */

/**
 * Created by sbt-kraynov-sa on 24.01.2018.
 */
public class TestErrorCollector extends ErrorCollector {

    public ArrayList<Throwable> getErrors() throws NoSuchFieldException, IllegalAccessException {
        Class cl = getClass().getSuperclass();
        Field fl = cl.getDeclaredField("errors");
        fl.setAccessible(true);
        return (ArrayList<Throwable>) fl.get(this);
    }

    public Boolean hasErrors() throws NoSuchFieldException, IllegalAccessException {
        return this.getErrors().size() != 0;
    }
}
