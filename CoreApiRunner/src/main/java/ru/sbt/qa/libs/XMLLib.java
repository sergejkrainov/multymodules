package ru.sbt.qa.libs;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;


public class XMLLib {

    private static final Logger LOG = Logger.getLogger(XMLLib.class);
    private static final String ERROR_MSG = "Не удалось сформировать объект типа Document";

    /**
     * Функция установки значения тега в xml документе
     */
    public static Document setXMLTagValue(Document xmlDocument, String tagName, String tagValue) throws Exception {
        NodeList nodeList = xmlDocument.getElementsByTagName(tagName);
        Element elList = (Element) nodeList.item(0);
        elList.setTextContent(tagValue);
        return xmlDocument;
    }

    /**
     * Функция конвертации xml в виде String в Document объект
     *
     * @param xmlContent - xml в виде строки
     * @param charset    - кодировка
     * @return
     * @throws ParserConfigurationException
     * @throws Exception
     */
    public static Document convertStringToDom(String xmlContent, String charset) throws Exception {
        //Преобразуем String xml шаблон в DOM документ
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        db = dbf.newDocumentBuilder();
        Document doc = null;
        try {
            doc = db.parse(new ByteArrayInputStream(xmlContent.getBytes(charset)));
        } catch (Exception e) {
            LOG.error(ERROR_MSG, new Exception(e));
        }
        return doc;
    }

    /**
     * Функция получения значения тега в xml Document
     *
     * @param docXML    - Document представление xml
     * @param fieldName - Название тега в xml
     * @return
     * @throws Exception
     */
    public static String getElementValueFromDocument(Document docXML, String fieldName) {
        String result = "";
        NodeList nodeList = docXML.getElementsByTagName(fieldName);
        Element elList = (Element) nodeList.item(0);
        result = elList.getTextContent();//Получили значение тега из ответа
        return result;
    }

    public static String formatPrettyXML(String unformattedXml, String charSet) throws Exception{
        try {
            final Document document = convertStringToDom(unformattedXml, charSet);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(65);
            format.setIndenting(true);
            format.setIndent(2);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);

            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Функция формирования xml к запросу
     *
     * @param xmlDoc                 DOM Document
     */
    public static String createXMLMessage(Document xmlDoc) throws Exception {
        String xmlMessage = "";
        try {
            xmlMessage = convertDomToString(xmlDoc);
            xmlMessage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xmlMessage;
            LOG.info("xmlMessage = " + xmlMessage);
        } catch (Exception e) {
            LOG.error("Не удалось сформировать xml-запрос");
            e.printStackTrace();
        }
        return xmlMessage;
    }

    /**
     * Функция формирования xml к запросу
     *
     * @param xmlDocument - Document объект для конвертации в String
     */
    public static String convertDomToString(Document xmlDocument) throws Exception {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
        String output = writer.getBuffer().toString();
        return output;
    }
}