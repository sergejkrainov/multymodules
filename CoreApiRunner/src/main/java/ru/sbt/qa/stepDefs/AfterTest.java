package ru.sbt.qa.stepDefs;

import cucumber.api.java.After;
import org.junit.Assert;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.AlmLogger;
import ru.sbt.qa.libs.ResourceUtil;
import ru.sbt.qa.libs.TestErrorCollector;

import java.util.ArrayList;
import java.util.Properties;

import static ru.sbt.qa.libs.AlmLogger.properties;

/**
 * Created by sbt-kraynov-sa on 20.02.2018.
 */
public class AfterTest {

    @Rule
    public static TestErrorCollector ec = new TestErrorCollector();

    protected static Properties almProperties;
    protected static String ALM_PROP_PATH = "/src/test/resources/config/" + "application-" + System.getProperty("occ.stand.name") + ".properties";;
    public static final String userPath = System.getProperty("user.dir");
    private static final Logger LOG = LoggerFactory.getLogger(BeforeTest.class);

    @After
    public void afterAllTest() throws Exception {
        almProperties = ResourceUtil.getPropertiesFromFile(userPath + ALM_PROP_PATH);
        properties.setProperty("alm.test.set.id",almProperties.getProperty("alm.test.set.id"));
        properties.setProperty("alm.test.id",almProperties.getProperty("alm.test.id"));
        properties.setProperty("alm.domain",almProperties.getProperty("alm.domain"));
        properties.setProperty("alm.project",almProperties.getProperty("alm.project"));
        System.out.println("alm.test.id=" + properties.getProperty("alm.test.id"));
        System.out.println("alm.test.set.id=" + properties.getProperty("alm.test.set.id"));
        System.out.println("alm.domain=" + properties.getProperty("alm.domain"));
        System.out.println("alm.project=" + properties.getProperty("alm.project"));
        if (properties != null && properties.getProperty("alm.test.id") != null) {
            if (ec.hasErrors()) {
                AlmLogger.almReport("Failed");
                printErrors();
                Assert.fail();
            } else {
                AlmLogger.almReport("Passed");
            }
        }
    }

    private void printErrors() throws NoSuchFieldException, IllegalAccessException {
        ArrayList<Throwable> err =  ec.getErrors();
        System.err.println("Errors in tests!");
        for(Throwable thr: err){
            System.err.println(thr.getMessage());
        }
    }


}
