package ru.sbt.qa.stepDefs;

import cucumber.api.java.Before;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.AssumptionViolatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BeforeTest {

    private static final Logger LOG = LoggerFactory.getLogger(BeforeTest.class);

    @Before
    public void setUp() throws IOException, AssumptionViolatedException {
        PropertyConfigurator.configure("../configs/log4j.properties");
        try {
            LOG.info("Инициализируем параметры запуска теста");
            System.setProperty("module-config", "src/test/resources/modules/module-config.properties");
            Properties properties = new Properties();
            try (InputStream streamFromResources = new FileInputStream("../configs/application.properties")) {
                properties.load(streamFromResources);
            } catch (IOException | NullPointerException e) {
                throw new RuntimeException("Failed to access properties file", e);
            }

            System.setProperty("skipAllTest", System.getProperty("skipAllTest", properties.getProperty("skipAllTest", "false")));
            String skipAllTest = (System.getProperty("skipAllTest"));
            if (skipAllTest != null && !skipAllTest.isEmpty()) {
                Assume.assumeFalse(Boolean.parseBoolean(skipAllTest));
            }

            System.setProperty("occ.stand.name", System.getProperty("occ.stand.name", properties.getProperty("occ.stand.name")));
            String stand = (System.getProperty("occ.stand.name"));

            LOG.info("occ.stand.name = " + stand);
            if (stand.isEmpty()) {
                throw new Error("Имя стенда не указано");
            } else {
                switch (stand.toLowerCase()) {
                    case "ift":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-ift.properties");
                        break;
                    case "st":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-st.properties");
                        break;
                    case "hermes":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-hermes.properties");
                        break;
                    case "zeus":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-zeus.properties");
                        break;
                    case "gefest":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-gefest.properties");
                        break;
                    case "psi":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-psi.properties");
                        break;
                    case "msv":
                        System.setProperty("platform-config", "../configs/platform-configs/platform-config-msv.properties");
                        break;
                    default:
                        throw new Error("Неправильно указано имя стенда - " + stand);
                }
            }
        } catch (AssumptionViolatedException assue) {
            LOG.info("Пропускаем ошибку, если стоит skipAllTest = true!\n", assue);
            throw assue;
        } catch (Exception e) {
            LOG.info("Не удалось преднастроить запуск тестов", e);
            Assert.fail("Не удалось преднастроить запуск тестов");
        }
    }
}
