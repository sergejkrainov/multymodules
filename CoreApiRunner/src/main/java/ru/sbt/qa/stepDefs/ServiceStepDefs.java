package ru.sbt.qa.stepDefs;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import ru.sbt.qa.ApiBaseTest;

import java.lang.reflect.Method;

/**
 * Created by out-mashnev-ma on 22.09.2017.
 */
public class ServiceStepDefs {
    private static ApiBaseTest testClassInstance;

    @Дано(value = "^Создан прокси транспорта для сервиса (.*?)$")
    public void createTransportProxy(String className) throws Throwable {
        String packageName = "ru.sbt.qa.";
        Class<?> testClass = Class.forName(packageName+className);
        Method instanceMethod = testClass.getDeclaredMethod("getInstance");
        testClassInstance = (ApiBaseTest) instanceMethod.invoke(null);
    }

    @Когда(value = "^Построить модель данных по xml$")
    public void buildXmlModel() throws Throwable {
        testClassInstance.createRequestModel();
    }

    @Когда(value = "^Отправить запрос$")
    public void sendRequest() throws Throwable {
        testClassInstance.sendRequest();
    }

    @Тогда(value = "^Проверить полученный ответ$")
    public void checkResponse() throws Throwable {
        testClassInstance.checkResponse();
    }
}