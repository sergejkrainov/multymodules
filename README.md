---
# Проект PPRB.ViS.api 
<br/>

## Запуск тестов осуществляется командной:

```mvn clean test -s settings.xml -DTAGS=@{TEST_TAG} -Docc.stand.name={STAND_NAME}```


## Сборка осуществляется командной:

```mvn clean package -Dmaven.test.skip=true```
```mvn clean install -Dmaven.test.skip=true```


## Сборка осуществляется командной (профили maven):
```mvn clean install -Pinstall```


## Запуск тестов осуществляется командной (профили maven):
```mvn clean test -Poip-cashless-common-operations```


## Запуск тестов осуществляется командной (профили maven):
```mvn clean test -Pdpf-editor -Dcucumber.options="--tags @Smoke" -Docc.stand.name=IFT```