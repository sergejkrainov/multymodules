package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.journal.creator.api.JournalCreatorApiRequest;
import ru.sbrf.journal.creator.api.dto.Context;
import ru.sbrf.journal.creator.api.dto.CreateJournalRs;
import ru.sbrf.journal.model.jms.Journal;
import ru.sbrf.journal.model.jms.JournalData;
import ru.sbrf.journal.model.jms.JournalHeader;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by out-mashnev-ma on 09.10.2017.
 */
public class ApplicationJournalCreator extends ApiBaseTest {

    private static ApplicationJournalCreator instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationJournalCreator.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private JournalCreatorApiRequest srvRequest;
    private Context context;
    private CreateJournalRs responseModel;

    private ApplicationJournalCreator() {
        srvRequest = createTransportProxy(JournalCreatorApiRequest.class);
    }

    public static ApplicationJournalCreator getInstance() {
        if (instance == null) {
            instance = new ApplicationJournalCreator();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        context = new Context();
        Journal journal = new Journal();
        journal.setHeader(new JournalHeader());
        journal.getHeader().setSystem("smokeTest");
        journal.getHeader().setIniciator("smoke");
        journal.getHeader().setEventDate(newXmlCalendar(new Date()));
        journal.getHeader().setOpDay(newXmlCalendar(new Date()));
        journal.getHeader().setHashKey("hashKey");
        journal.getHeader().setServiceId(java.util.UUID.randomUUID().toString());
        JournalData jd = new JournalData();
        jd.setOrder(777);
        jd.setDataType("KMD");
        jd.setDataObject("smokeTestKMD");
        journal.getJournalData().add(jd);
        context.put("journal", journal);
    }

    public void sendRequest() throws Exception {

        try {
            Request<CreateJournalRs> request = srvRequest.createJournalWithResult("VIS111", context);
            responseModel = request.onModule("application-journal-creator")
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("CreateJournalRs: Код ошибки = " + responseModel.getErrCode());
            LOG.info("CreateJournalRs: Текст ошибки = " + responseModel.getErrMessage());
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getErrCode() != null, Matchers.is(true));
        }


    }
    static XMLGregorianCalendar newXmlCalendar(Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        XMLGregorianCalendar result = null;
        try {
            result = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (DatatypeConfigurationException e) {
        }
        return result;
    }
}