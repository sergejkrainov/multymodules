#language:ru
@All @smoke
Функционал: {application-journal-creator}

  @application-journal-creator
  @JournalCreatorApiRequest
  Сценарий: {JournalCreatorApiRequest}
    Дано Создан прокси транспорта для сервиса ApplicationJournalCreator
    * Построить модель данных по xml
    * Отправить запрос
    * Проверить полученный ответ
