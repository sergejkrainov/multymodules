package ru.sbt.qa;

import com.sbt.pprb.sos.autotrans.api.Account_SetProperty_For_AutosubscriptionRequest;
import com.sbt.pprb.sos.autotrans.common.SubscribeStatus;
import com.sbt.pprb.sos.autotrans.common.SubscriptionType;
import org.junit.Assert;

public class AutoSubscriptionApiSubscriptionByAccount extends ApiTestBaseWithClientAndDepositCreation {
    private static AutoSubscriptionApiSubscriptionByAccount instance;

    /**
     * Параметры запроса
     */
    private String account;
    private int codeTb;
    private SubscriptionType subscriptionType;

    /**
     * Инициализация используемых API
     */
    private Account_SetProperty_For_AutosubscriptionRequest account_setProperty_for_autosubscriptionRequest = createTransportProxy(Account_SetProperty_For_AutosubscriptionRequest.class);

    private SubscribeStatus subscribeStatus;

    private AutoSubscriptionApiSubscriptionByAccount() throws Exception {
    }

    public static AutoSubscriptionApiSubscriptionByAccount getInstance() throws Exception {
        if (instance == null) {
            instance = new AutoSubscriptionApiSubscriptionByAccount();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        /**
         * Параметры отбивки в ALM
         */
        ALM_PROP_PATH = "/src/test/resources/alm/AutoSubscriptionApiSubscriptionByAccount.properties";

        /**
         * Задание параметров запроса
         */
        account = product.getContract().getNumber();
        LOG.info("Параметр account \"" + account + "\"");
        codeTb = Integer.parseInt(product.getDivision().getTerBank());
        LOG.info("Параметр codeTb \"" + codeTb + "\"");
        subscriptionType = SubscriptionType.CREDIT_TYPE;
        LOG.info("Параметр SubscriptionType \"" + subscriptionType + "\"");
    }

    @Override
    public void sendRequest() throws Exception {
        LOG.info("Отправка запроса \"" + Account_SetProperty_For_AutosubscriptionRequest.class.getSimpleName() + "\"");
        subscribeStatus = account_setProperty_for_autosubscriptionRequest.subscribe(account, codeTb, subscriptionType).execute();
    }

    @Override
    public void checkResponse() throws Exception {
        LOG.info("Проверка SubscribeStatus");
        LOG.info("Код ответа \"" + subscribeStatus.getCode() + "\"");
        Assert.assertEquals("API сработало с ошибкой", 0, subscribeStatus.getCode());
    }
}