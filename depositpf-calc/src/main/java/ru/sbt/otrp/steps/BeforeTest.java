package ru.sbt.otrp.steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import ru.sbt.otrp.helpers.Props;
import static ru.sbt.otrp.steps.Steps.props;

/**
 * Created by sbt-bratuhin-myu on 25.09.2017.
 */

public class BeforeTest {

    @Before
    public void setUp() {
        props = new Props();
    }

    @After
    public void wasError() {
    }
}
