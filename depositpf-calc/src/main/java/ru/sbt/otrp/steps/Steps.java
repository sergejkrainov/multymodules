package ru.sbt.otrp.steps;

import cucumber.api.java.ru.Дано;
import org.junit.Assert;
import ru.sbt.otrp.helpers.Props;
import ru.sbt.qa.libs.HttpURLConnectionLib;

import java.net.URL;

/**
 * Created by sbt-bratuhin-myu on 28.06.2017.
 */
public class Steps {

    public static Props props;

    @Дано("^Отправляем запрос$")
    public void отправляем_запрос() throws Throwable {
        String url = (System.getProperty("rest.starting.url")==null || (System.getProperty("rest.starting.url").isEmpty()))?props.get("rest.starting.url"):System.getProperty("rest.starting.url");
        url = (url.endsWith("/"))?url:url+"/";
        url = (url.endsWith("dpfcalc/"))?url:url+"dpfcalc/";
        HttpURLConnectionLib.HttpMessageResponse resp;
        try {
            resp = HttpURLConnectionLib.sendGet(new URL(url), "");
            System.out.println("resp.URL = " + resp.URL);
            System.out.println("resp.ResponseCode = " + resp.ResponseCode);
            System.out.println("resp.ResponseMessage = " + resp.ResponseMessage);
            System.out.println("resp.ResponseBody = " + resp.ResponseBody);
            Assert.assertNotEquals(404, resp.ResponseCode.intValue());
            Assert.assertTrue("Страница содержит искомый текст", resp.ResponseBody.contains("Фабрика вкладов. Сервисы для расчёта финансовых операций"));
        } catch (Exception e){
            Assert.assertTrue("Error! " + e.getMessage(), false);
        }
    }
}
