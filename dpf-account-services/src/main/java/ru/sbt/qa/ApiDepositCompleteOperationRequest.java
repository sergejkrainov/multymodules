package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.complete.DepositCompleteOperationRequest;
import ru.sbrf.depositpf.service.model.DepositCompleteRequest;
import ru.sbrf.depositpf.service.model.DepositCompleteResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositCompleteOperationRequest extends ApiBaseTest{

    private static ApiDepositCompleteOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositCompleteOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositCompleteOperationRequest srvRequest;
    private DepositCompleteRequest requestModel;
    private DepositCompleteResponse responseModel;

    private ApiDepositCompleteOperationRequest() {
        srvRequest = createTransportProxy(DepositCompleteOperationRequest.class);
    }

    public static ApiDepositCompleteOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositCompleteOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {


        DepositCompleteRequest rq = new DepositCompleteRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositCompleteResponse> request = srvRequest.depositComplete(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}