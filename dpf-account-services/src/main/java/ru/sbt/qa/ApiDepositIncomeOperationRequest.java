package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.income.DepositIncomeOperationRequest;
import ru.sbrf.depositpf.service.model.DepositIncomeRequest;
import ru.sbrf.depositpf.service.model.DepositIncomeResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositIncomeOperationRequest extends ApiBaseTest{

    private static ApiDepositIncomeOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositIncomeOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositIncomeOperationRequest srvRequest;
    private DepositIncomeRequest requestModel;
    private DepositIncomeResponse responseModel;

    private ApiDepositIncomeOperationRequest() {
        srvRequest = createTransportProxy(DepositIncomeOperationRequest.class);
    }

    public static ApiDepositIncomeOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositIncomeOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {


        DepositIncomeRequest rq = new DepositIncomeRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositIncomeResponse> request = srvRequest.depositIncome(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}