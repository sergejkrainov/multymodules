package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.payment.DepositPaymentOperationRequest;
import ru.sbrf.depositpf.service.model.DepositPaymentRequest;
import ru.sbrf.depositpf.service.model.DepositPaymentResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositPaymentOperationRequest extends ApiBaseTest{

    private static ApiDepositPaymentOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositPaymentOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositPaymentOperationRequest srvRequest;
    private DepositPaymentRequest requestModel;
    private DepositPaymentResponse responseModel;

    private ApiDepositPaymentOperationRequest() {
        srvRequest = createTransportProxy(DepositPaymentOperationRequest.class);
    }

    public static ApiDepositPaymentOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositPaymentOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        DepositPaymentRequest rq = new DepositPaymentRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositPaymentResponse > request = srvRequest.depositPayment(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}