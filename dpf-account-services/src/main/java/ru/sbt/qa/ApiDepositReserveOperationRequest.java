package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.reserve.DepositReserveOperationRequest;
import ru.sbrf.depositpf.service.model.DepositReserveRequest;
import ru.sbrf.depositpf.service.model.DepositReserveResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositReserveOperationRequest extends ApiBaseTest{

    private static ApiDepositReserveOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositReserveOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositReserveOperationRequest srvRequest;
    private DepositReserveRequest requestModel;
    private DepositReserveResponse responseModel;

    private ApiDepositReserveOperationRequest() {
        srvRequest = createTransportProxy(DepositReserveOperationRequest.class);
    }

    public static ApiDepositReserveOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositReserveOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        DepositReserveRequest rq = new DepositReserveRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositReserveResponse > request = srvRequest.depositReserve(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}