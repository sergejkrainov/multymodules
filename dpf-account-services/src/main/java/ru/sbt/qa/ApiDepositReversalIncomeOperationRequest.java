package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.reversalincome.DepositReversalIncomeOperationRequest;
import ru.sbrf.depositpf.service.model.DepositReversalIncomeRequest;
import ru.sbrf.depositpf.service.model.DepositReversalIncomeResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositReversalIncomeOperationRequest extends ApiBaseTest{

    private static ApiDepositReversalIncomeOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositReversalIncomeOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositReversalIncomeOperationRequest srvRequest;
    private DepositReversalIncomeRequest requestModel;
    private DepositReversalIncomeResponse responseModel;

    private ApiDepositReversalIncomeOperationRequest() {
        srvRequest = createTransportProxy(DepositReversalIncomeOperationRequest.class);
    }

    public static ApiDepositReversalIncomeOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositReversalIncomeOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        DepositReversalIncomeRequest rq = new DepositReversalIncomeRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositReversalIncomeResponse> request = srvRequest.reversalIncome(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}