package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbrf.depositpf.service.api.reversalpayment.DepositReversalPaymentOperationRequest;
import ru.sbrf.depositpf.service.model.DepositReversalPaymentRequest;
import ru.sbrf.depositpf.service.model.DepositReversalPaymentResponse;

import java.util.concurrent.TimeUnit;

public class ApiDepositReversalPaymentOperationRequest extends ApiBaseTest{

    private static ApiDepositReversalPaymentOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositReversalPaymentOperationRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositReversalPaymentOperationRequest srvRequest;
    private DepositReversalPaymentRequest requestModel;
    private DepositReversalPaymentResponse responseModel;

    private ApiDepositReversalPaymentOperationRequest() {
        srvRequest = createTransportProxy(DepositReversalPaymentOperationRequest.class);
    }

    public static ApiDepositReversalPaymentOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositReversalPaymentOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        DepositReversalPaymentRequest rq = new DepositReversalPaymentRequest();
        rq.setServiceId("serviceId");
        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<DepositReversalPaymentResponse> request = srvRequest.reversalPayment(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account-services").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Код ответа на вызов операции:\n\n" + responseModel.getResult().getCode(), "UTF-8");
        }
    }
}