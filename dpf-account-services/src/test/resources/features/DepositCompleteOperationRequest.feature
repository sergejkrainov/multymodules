#language:ru
@All @smoke
Функционал: {dpf-account-services}

  @dpf-account-services
  @DepositCompleteOperationRequest
  Сценарий: {DepositCompleteOperationRequest}
    Дано Создан прокси транспорта для сервиса ApiDepositCompleteOperationRequest
    * Построить модель данных по xml
    * Отправить запрос
    * Проверить полученный ответ
