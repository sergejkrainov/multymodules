package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.services.api.dpfaccount.opentransactioncredit.OpenTransactionCreditRequest;
import com.sbt.pprb.services.api.dpfaccount.opentransactioncredit.dto.OpenTransactionCreditRq;
import com.sbt.pprb.services.api.dpfaccount.opentransactioncredit.dto.OpenTransactionCreditRs;
import com.sbt.pprb.services.api.dpfaccount.reservetransactioncredit.dto.MainInfo;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class ApiOpenTransactionCreditRequest extends ApiBaseTest{

    private static ApiOpenTransactionCreditRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiOpenTransactionCreditRequest.class);

    private String rqUID;
    private String operUID;
    private long TIMEOUT_IN_SECONDS = 60;
    private OpenTransactionCreditRequest srvRequest;
    private OpenTransactionCreditRq requestModel;
    private OpenTransactionCreditRs responseModel;

    private ApiOpenTransactionCreditRequest() {
        srvRequest = createTransportProxy(OpenTransactionCreditRequest.class);
    }

    public static ApiOpenTransactionCreditRequest getInstance() {
        if (instance == null) {
            instance = new ApiOpenTransactionCreditRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        OpenTransactionCreditRq rq = new OpenTransactionCreditRq();
        MainInfo mainInfo = new MainInfo();
        mainInfo.setDepositCurrency(810);
        rq.setServiceId("123");
        rq.setProductId("ProductID");

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        rq.setOperUID(operUID);
        rq.setRqUID(rqUID);
        rq.setRqTm(new GregorianCalendar());

        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<OpenTransactionCreditRs> request = srvRequest.openTransactionCredit(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Статус ответа на вызов операции:\n\n" + responseModel.getStatus().getStatusDesc(), "UTF-8");
            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + responseModel.getOperUID(),
                    operUID, Matchers.equalTo(responseModel.getOperUID()));
        }
    }
}