package ru.sbt.qa;


import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.services.api.dpfaccount.reservetransactioncredit.ReserveTransactionCreditRequest;
import com.sbt.pprb.services.api.dpfaccount.reservetransactioncredit.dto.*;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class ApiReserveTransactionCreditRequest extends ApiBaseTest{

    private static ApiReserveTransactionCreditRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiReserveTransactionCreditRequest.class);

    private String rqUID;
    private String operUID;
    private long TIMEOUT_IN_SECONDS = 60;
    private ReserveTransactionCreditRequest srvRequest;
    private ReserveTransactionCreditRq requestModel;
    private ReserveTransactionCreditRs responseModel;

    private ApiReserveTransactionCreditRequest() {
        srvRequest = createTransportProxy(ReserveTransactionCreditRequest.class);
    }

    public static ApiReserveTransactionCreditRequest getInstance() {
        if (instance == null) {
            instance = new ApiReserveTransactionCreditRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        ReserveTransactionCreditRq rq = new ReserveTransactionCreditRq();
        rq.setOperUID(operUID);
        rq.setRqUID(rqUID);
        rq.setRqTm(new GregorianCalendar());

        MainInfo mainInfo = new MainInfo();
        mainInfo.setDepositCurrency(810);
        rq.setMainInfo(mainInfo);

        rq.setClientPartyId("1108222102389480105");
        rq.setOperationDay(new GregorianCalendar());
        rq.setSubsystem("Subsystem");
        rq.setPatternId("172");
        rq.setServiceId("123");

        OfficeInfo officeInfo = new OfficeInfo(38, 9038, 166);
        rq.setOfficeInfo(officeInfo);

        OperatorInfo operatorInfo = new OperatorInfo();
        operatorInfo.setLogin("Логин оператора");
        operatorInfo.setCode("Код оператора");
        operatorInfo.setKeyId("ID электронного ключа оператора");
        operatorInfo.setName("ФИО оператора");
        operatorInfo.setPost("Должность оператора");
        rq.setOperatorInfo(operatorInfo);

        requestModel = rq;

        LOG.info("Сформатированная xml модель для отправки:\n\n" + requestModel.toString(), "UTF-8");
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<ReserveTransactionCreditRs> request = srvRequest.reserveTransactionCredit(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-account").execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Статус ответа на вызов операции:\n\n" + responseModel.getStatus().getStatusDesc(), "UTF-8");
            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + responseModel.getOperUID(),
                    operUID, Matchers.equalTo(responseModel.getOperUID()));
        }
    }
}