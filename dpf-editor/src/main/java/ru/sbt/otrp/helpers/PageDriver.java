package ru.sbt.otrp.helpers;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;

import static ru.sbt.otrp.pages.AbstractPage.props;

/**
 * Created by sbt-bratuhin-myu on 25.09.2017.
 */
public class PageDriver {

    private static WebDriver webDriver;

    private static final String WEBDRIVER_PATH = System.getProperty("user.dir")
            + File.separator + "src"
            + File.separator + "test"
            + File.separator + "resources"
            + File.separator + "webdrivers"
            + File.separator + getOsArchitecture()
            + File.separator + "IEDriverServer.exe";

    private static String getOsArchitecture(){
        // FIXME: 17.01.2018 убрал х64 версию из-за жутких тормозов при вводе (решение пока не нагуглил, но многие жалуются на тоже самое)
//        String arch = System.getProperty("os.arch");
//        if (arch != null && arch.contains("64")) {
//            return "x64";
//        } else {
//            return "x32";
//        }
        return "x32";
    }

    public static WebDriver getDriver() throws Exception {
        if (webDriver == null) {
            createDriver();
        }
        return webDriver;
    }

    public static void createDriver() throws Exception {
        System.setProperty("webdriver.ie.driver", WEBDRIVER_PATH);
        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        caps.setCapability("requiredWindowFocus", true);
        caps.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        caps.setCapability("ignoreProtectedModeSettings", true);
        caps.setCapability("ignoreZoomSetting", true);
        caps.setCapability("nativeEvents", false);
        caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        caps.setCapability("disable-popup-blocking", true);
        caps.setCapability("enablePersistentHover", true);

        System.out.println("WEBDRIVER_PATH = " + WEBDRIVER_PATH);
        // FIXME: 17.01.2018 обновить
        InternetExplorerOptions options = new InternetExplorerOptions(caps);
        webDriver = new InternetExplorerDriver(options);
    }

    public static void dispose() {
        if (webDriver != null) {
            webDriver.close();
        }
        killIE();
    }

    private static void killIE() {
        if ("IE".equals(props.get("browser.name","IE"))) {

            try {
                Runtime.getRuntime().exec("taskkill /f /im iexplore.exe");
            } catch (Error | Exception var7) {
                System.err.println("Failed to kill all of the iexplore processes: " + var7);
            }

            try {
                Runtime.getRuntime().exec("taskkill /f /im IEDriverServer.exe");
            } catch (Error | Exception var7) {
                System.err.println("Failed to kill all of the IEDriverServer processes: " + var7);
            }
        }
    }
}
