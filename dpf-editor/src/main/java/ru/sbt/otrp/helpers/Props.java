package ru.sbt.otrp.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by sbt-bratuhin-myu on 26.09.2017.
 */
public class Props {

    Properties properties = new Properties();

    public Props(){
        String standName = "-";
        if (System.getProperty("occ.stand.name") != null) {
            standName = standName + System.getProperty("occ.stand.name");
        } else {
            standName = "-TEST";
        }
        System.out.println("Выполняем загрузку данных из конфиг файла = config/application" + standName.toUpperCase() + ".properties");
        try (InputStream streamFromResources = Props.class.getClassLoader().getResourceAsStream("config/application"+standName.toUpperCase() + ".properties")) {
            InputStreamReader isr = new InputStreamReader(streamFromResources, "UTF-8");
            this.properties.load(isr);
            System.out.println("Загрузка успешно выполнена!\r\n" + properties);
        } catch (IOException | NullPointerException e) {
            throw new RuntimeException("Failed to access properties file", e);
        }
    }

    public String get(String key){
        if (properties != null) {
            return this.properties.getProperty(key);
        } else {
            throw new RuntimeException("Не заргужен application.properties!");
        }
    }

    public String get(String key, String defaultValue){
        if (properties != null) {
            return this.properties.getProperty(key, defaultValue);
        } else {
            throw new RuntimeException("Не заргужен application.properties!");
        }
    }
}
