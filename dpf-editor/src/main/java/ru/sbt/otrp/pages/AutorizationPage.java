package ru.sbt.otrp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ru.sbt.otrp.helpers.PageDriver;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Created by sbt-bratuhin-myu on 25.09.2017.
 */
public class AutorizationPage extends AbstractPage{

    private WebElement domen;
    private WebElement login;
    private WebElement password;
    private WebElement submitButton;

    public void setDomen(WebDriver webDriver) throws InterruptedException {
        if (props.get("xpath.domen") != null && !"".equals(props.get("xpath.domen"))) {
            Thread.sleep(100L);
            domen = webDriver.findElement(By.xpath(props.get("xpath.domen")));
            domen.clear();
            domen.sendKeys(props.get("domen"));
            System.out.println("Вводим домен = " + props.get("domen"));
        }
    }

    public void setLogin(WebDriver webDriver) throws InterruptedException {
        if (props.get("xpath.login") != null && !"".equals(props.get("xpath.login"))) {
            Thread.sleep(100L);
            login = webDriver.findElement(By.xpath(props.get("xpath.login")));
            login.clear();
            login.sendKeys(props.get("login"));
            System.out.println("Вводим логин = " + props.get("login"));
        }
    }

    public void setPassword(WebDriver webDriver) throws InterruptedException {
        if (props.get("xpath.password") != null && !"".equals(props.get("xpath.password"))) {
            Thread.sleep(100L);
            password = webDriver.findElement(By.xpath(props.get("xpath.password")));
            password.clear();
            password.sendKeys(props.get("password"));
            System.out.println("Вводим пароль = " + props.get("password"));
        }
    }

    public void submit(WebDriver webDriver) throws InterruptedException {
        if (props.get("xpath.button") != null && !"".equals(props.get("xpath.button"))) {
            Thread.sleep(100L);
            submitButton = webDriver.findElement(By.xpath(props.get("xpath.button")));
            String windowHandle = webDriver.getWindowHandle();
            System.out.println("WindowHandles old = " + webDriver.getWindowHandles());
            submitButton.click();
            System.out.println("Нажимаем кнопку регистрация");
            System.out.println("WindowHandles new = " + webDriver.getWindowHandles());
            webDriver.switchTo().window(windowHandle);
        }
    }
}
