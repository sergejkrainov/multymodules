package ru.sbt.otrp.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by sbt-bratuhin-myu on 26.09.2017.
 */
public class MainPage extends AbstractPage {

    public String getCurrentUrl(WebDriver webDriver) throws Throwable{
        webDriver.switchTo().window(webDriver.getWindowHandle());

        System.out.println("Мы находимся на странице: " + webDriver.getCurrentUrl());
        return webDriver.getCurrentUrl();
    }

    public void checkElementPage(WebDriver webDriver) throws Throwable {
        Thread.sleep(100L);
        WebElement buttonFind = webDriver.findElement(By.xpath(props.get("xpath.buttonFind")));
        Assert.assertTrue("Проверка видимости кнопки \"Найти\" на главной странице редактора вкладов", buttonFind.isDisplayed());
        System.out.println("Найден элемент на странице: button=\"" + buttonFind.getText() + "\"");
    }

    public void checkRestServices(WebDriver webDriver) throws Throwable {
        Thread.sleep(100L);
        System.out.println("Проверяем запрос на поиск продукта (через REST-запрос - /rest/searchProduct?productId=1");
        System.out.println("getCurrentUrl = " + getCurrentUrl(webDriver));
        String s = getCurrentUrl(webDriver) + "rest/searchProduct?productId=1";
        System.out.println("rest = " + s);
        webDriver.navigate().to(s);
        System.out.println("getPageSource:\r\n" + webDriver.getPageSource());
    }
}
