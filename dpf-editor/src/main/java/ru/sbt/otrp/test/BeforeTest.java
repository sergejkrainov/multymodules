package ru.sbt.otrp.test;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import ru.sbt.otrp.helpers.PageDriver;
import ru.sbt.otrp.helpers.Props;

import static ru.sbt.otrp.pages.AbstractPage.props;

/**
 * Created by sbt-bratuhin-myu on 25.09.2017.
 */

public class BeforeTest {

    @Before
    public void setUp() {
         props = new Props();
    }

    @After
    public void wasError() {
        if (props.get("browser.name") != null) {
            System.out.println("Выполняем after метод, убиваем драйвер!");
            PageDriver.dispose();
        }
    }
}
