package ru.sbt.otrp.test;

import cucumber.api.PendingException;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import ru.sbt.otrp.helpers.HttpURLConnectionLib;
import ru.sbt.otrp.helpers.PageDriver;
import ru.sbt.otrp.pages.AutorizationPage;
import ru.sbt.otrp.pages.MainPage;

import java.net.URL;

import static ru.sbt.otrp.pages.AbstractPage.props;

/**
 * Created by sbt-bratuhin-myu on 28.06.2017.
 */
public class Steps {

    private AutorizationPage autorizationPage;
    private WebDriver webDriver;

    @Дано("^открыта стартовая страница приложения \"([^\"]*)\"$")
    public void открыта_стартовая_страница_приложения(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        autorizationPage = new AutorizationPage();
        webDriver = PageDriver.getDriver();
        System.out.println("Стартовая страница = " + props.get("webdriver.starting.url"));
        webDriver.navigate().to(props.get("webdriver.starting.url"));
        Thread.sleep(5000L);
        if (webDriver.getTitle().contains("Ошибка сертификата")) {
            webDriver.navigate().to("javascript:document.getElementById('overridelink').click()");
            Thread.sleep(2000L);
        }
    }

    @Дано("^пользователь вводит \"([^\"]*)\"$")
    public void пользователь_вводит(String arg1) throws Throwable {
        switch (arg1){
            case "домен": {autorizationPage.setDomen(webDriver); break;}
            case "логин": {autorizationPage.setLogin(webDriver); break;}
            case "пароль": {autorizationPage.setPassword(webDriver); break;}
            default: throw new PendingException("Некорректнная реализация метода!");
        }
    }

    @Дано("^пользователь нажимает кнопку \"([^\"]*)\"$")
    public void пользователь_нажимает_кнопку(String arg1) throws Throwable {
        autorizationPage.submit(webDriver);
    }

    @Тогда("^пользователь авториазован в системе$")
    public void пользователь_авториазован_в_системе() throws Throwable {
        MainPage mainPage = new MainPage();
        if (mainPage.getCurrentUrl(webDriver).equals(props.get("webdriver.pkmslogin.url"))){
            Thread.sleep(2000L);
            System.out.println("Перезапускаем страницу");
            webDriver.navigate().to(props.get("webdriver.starting.url"));
        }
        System.out.println("Находимся на главной странице приложения = " + props.get("webdriver.main.url"));
        mainPage.checkElementPage(webDriver);
        mainPage.checkRestServices(webDriver);
    }

    @Дано("^Отправляем запрос \"([^\"]*)\"$")
    public void отправляем_запрос(String arg1) throws Throwable {
        String url = System.getProperty("rest.starting.url", props.get("rest.starting.url"));
        url = (url.endsWith("/"))?url:url+"/";
        HttpURLConnectionLib.HttpMessageResponse resp;
        try {
            resp = HttpURLConnectionLib.sendGet(new URL(url), "");
            System.out.println("resp.URL = " + resp.URL);
            System.out.println("resp.ResponseCode = " + resp.ResponseCode);
            System.out.println("resp.ResponseMessage = " + resp.ResponseMessage);
            System.out.println("resp.ResponseBody = " + resp.ResponseBody);
            Assert.assertNotEquals(404, resp.ResponseCode.intValue());
        } catch (Exception e){
            Assert.assertTrue("Error! " + e.getMessage(), false);
        }

        try {
            resp = HttpURLConnectionLib.sendGet(new URL(url), "rest/" + arg1);
            System.out.println("resp.URL = " + resp.URL);
            System.out.println("resp.ResponseCode = " + resp.ResponseCode);
            System.out.println("resp.ResponseMessage = " + resp.ResponseMessage);
            System.out.println("resp.ResponseBody = " + resp.ResponseBody);
            Assert.assertNotEquals(404, resp.ResponseCode.intValue());
        } catch (Exception e){
            Assert.assertTrue("Error! " + e.getMessage(), false);
        }
    }
}
