package actions;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static variables.CalculatedVariables.loginPage;
import static variables.CalculatedVariables.webDriver;
import static variables.CalculatedVariables.webDriverWait;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */
public class UiActions {
    private static final Logger LOG = LoggerFactory.getLogger(UiActions.class);

    public static void open(String url) {
        LOG.info("Открытие URL \"" + url + "\"");

        webDriver.get(url);

        LOG.info("Название страницы \"" + webDriver.getTitle() + "\"");
        LOG.info("Расширение экрана");

        webDriver.manage().window().maximize();

        /**
         * Проверяем, есть ли ошибка сертификата
         */
        if (webDriver.getTitle().contains("Ошибка сертификата") || webDriver.getTitle().contains("Certificate Error")) {
            webDriver.navigate().to("javascript:document.getElementById('overridelink').click()");
            try {
                Thread.sleep(2000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void login(String url, String username, String password) throws Exception {
        LOG.info("Открытие страницы входа");

        open(url);

        LOG.info("Ввод данных пользователя");
        LOG.info("Логин \"" + username + "\"");

        loginPage.usernameField.clear();
        loginPage.usernameField.sendKeys(username);

        LOG.info("Пароль \"" + password + "\"");

        loginPage.passwordField.clear();
        loginPage.passwordField.sendKeys(password);

        LOG.info("Нажатие кнопки входа");

        loginPage.enterButton.click();
    }

    public static void checkMainPage() {
        webDriverWait.until(ExpectedConditions.titleIs("Просмотр списка реестров - АС Back-office ФП Безналичные расчёты"));

        LOG.info("Проверка названия страницы");

        Assert.assertEquals("Название не совпало", webDriver.getTitle(), "Просмотр списка реестров - АС Back-office ФП Безналичные расчёты");
    }
}
