package helpers;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.LoginPage;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

import static variables.CalculatedVariables.*;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */
public class InitHelper {
    private static Logger LOG = LoggerFactory.getLogger(InitHelper.class);

    public static void initPages() {
        LOG.info("Инициализация страниц");

        loginPage = new LoginPage();
        mainPage = new MainPage();
    }

    public static void initBrowser() {
        LOG.info("Инициализация браузера");

        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\main\\driver\\IEDriverServer.exe");

        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();

        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        caps.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        caps.setCapability("ignoreProtectedModeSettings", true);
        caps.setCapability("ignoreZoomSetting", true);
        caps.setCapability("nativeEvents", false);
        caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

        InternetExplorerOptions options = new InternetExplorerOptions(caps);

        webDriver = new InternetExplorerDriver(options);
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        webDriverWait = new WebDriverWait(webDriver, 30);
    }
}
