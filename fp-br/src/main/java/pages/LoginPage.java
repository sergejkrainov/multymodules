package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */

public class LoginPage extends Page {
    /**
     * Поле "Домен"
     */
    @FindBy(xpath = "//input[@id='domain']")
    public WebElement domainField;

    /**
     * Поле "Имя пользователя"
     */
    @FindBy(xpath = "//input[@id='j_username']")
    public WebElement usernameField;

    /**
     * Поле "Пароль"
     */
    @FindBy(xpath = "//input[@id='j_password']")
    public WebElement passwordField;

    /**
     * Кнопка "Войти"
     */
    @FindBy(xpath = "//input[@value='Войти']")
    public WebElement enterButton;

    /**
     * Кнопка "Войти по ТМ"
     */
    @FindBy(xpath = "//input[@value='Войти по ТМ']")
    public WebElement enterByTmButton;
}
