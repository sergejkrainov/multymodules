package pages;

import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static variables.CalculatedVariables.webDriver;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */

public class Page {
    protected Logger LOG = LoggerFactory.getLogger(Page.class);

    public Page() {
        PageFactory.initElements(webDriver, this);
    }
}
