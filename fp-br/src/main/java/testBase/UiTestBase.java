package testBase;

import helpers.InitHelper;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static variables.CalculatedVariables.webDriver;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */
public class UiTestBase {
    protected Logger LOG = LoggerFactory.getLogger(UiTestBase.class);

    @Before
    public void initTest() throws Exception {
        LOG.info("Инициализация теста");
        LOG.info("Завершение существующих процессов WebDriver");

        try {
            Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe").waitFor();
        } catch (Exception e) {
            LOG.debug("Не удалось завершить процессы WebDriver");
        }

        InitHelper.initBrowser();
        InitHelper.initPages();
    }

    @After
    public void afterTest() {
        LOG.info("Закрытие браузера");

        webDriver.quit();

        LOG.info("Завершение процессов WebDriver");

        try {
            Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe").waitFor();
        } catch (Exception e) {
            LOG.debug("Не удалось завершить процессы WebDriver");
        }

        LOG.info("Завершение теста");
    }
}
