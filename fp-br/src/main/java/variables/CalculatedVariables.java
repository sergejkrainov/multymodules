package variables;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.LoginPage;
import pages.MainPage;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */
public class CalculatedVariables {
    public static WebDriver webDriver;
    public static WebDriverWait webDriverWait;

    public static LoginPage loginPage;
    public static MainPage mainPage;
}
