package ui;

import actions.UiActions;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import testBase.UiTestBase;
import variables.Config;

/**
 * Created by out-oreshin-aa on 14.03.2018.
 */
public class IftSmoke extends UiTestBase {
    private Logger LOG = LoggerFactory.getLogger(StSmoke.class);

    @Test
    public void test() throws Exception {
        UiActions.login(Config.iftUrl, Config.iftUsername, Config.iftPassword);
        UiActions.checkMainPage();
    }
}
