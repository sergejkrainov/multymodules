package ru.sbt.qa;

import com.sbt.pprb.uts.moneybox.api.SrvMoneyboxNotifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by sbt-sharikov-pi on 09.10.2017.
 */
public class SrvMoneyBoxNotifierApi extends ApiBaseTest {
    private static SrvMoneyBoxNotifierApi instance;
    private static final Logger LOG = LoggerFactory.getLogger(SrvMoneyBoxNotifierApi.class);

    private String rqUID;
    private String operUID;
    private long TIMEOUT_IN_SECONDS = 60;
    private long SLEEP_IN_MILLISECONDS = 120000;
    private SrvMoneyboxNotifier srvMoneyBoxNotifier;

    private SrvMoneyBoxNotifierApi() {
        srvMoneyBoxNotifier = createTransportProxy(SrvMoneyboxNotifier.class);
    }

    public static SrvMoneyBoxNotifierApi getInstance() {
        if (instance == null) {
            instance = new SrvMoneyBoxNotifierApi();
        }
        return instance;
    }

    public void createRequestModel() {

    }

    @Override
    public void sendRequest() throws Exception {
        try {
/*            Thread.sleep(SLEEP_IN_MILLISECONDS);
            Request<InitiateChangeTariffCardRs> request = srvMoneyBoxNotifier.invoke(requestModel);
            response = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();*/
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
    }
}
