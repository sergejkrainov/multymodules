package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.Message;
import com.sbt.pprb.dto.accdi.TransferResponse;
import com.sbt.pprb.services.api.pprb.tddn.TDDNRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by sbt-sharikov-pi on 20.10.2017.
 */
public class ApiAccToAccClose143NominalRequest extends ApiBaseTest {

    private static ApiAccToAccClose143NominalRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiSrvCardToPaymentRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private TDDNRequest srvRequest;
    private com.sbt.pprb.dto.accdi.Message requestModel;
    private TransferResponse responseModel;
    private String uuid;

    private ApiAccToAccClose143NominalRequest() {
        srvRequest = createTransportProxy(TDDNRequest.class);
    }

    public static ApiAccToAccClose143NominalRequest getInstance() {
        if (instance == null) {
            instance = new ApiAccToAccClose143NominalRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        File templateXml = new File("src/test/resources/xml/accToAccClose143NominalRequest.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (Message) parser.getObject(templateXml, Message.class);

        uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar operDate = Calendar.getInstance();

        requestModel.getAccToAccClose143NominalRequest().setUser(requestModel.getAccToAccClose143NominalRequest().getUser().withUUID(uuid));
        requestModel.getAccToAccClose143NominalRequest().setUser(requestModel.getAccToAccClose143NominalRequest().getUser().withOperDate(operDate));

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        Map<String, Object> mapa = new LinkedHashMap<String, Object>();
        try {
            Request<TransferResponse> request = srvRequest.accToAccClose143Nominal(requestModel.getAccToAccClose143NominalRequest(), mapa);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            System.out.println("TransferResponse: Код ошибки = " + responseModel.getError());
            LOG.info("TransferResponse: Текст ошибки = " + responseModel.getError());
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getError() != null, Matchers.is(true));
        }
    }
}