package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.TransferResponse;
import com.sbt.pprb.services.api.pprb.tddc.atac143.ATAC143Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

//import com.sbt.pprb.dto.accdi.Message;

/**
 * Created by sbt-sharikov-pi on 19.10.2017.
 */
public class ApiAccToAccClose143Request extends ApiBaseTest {

    private static ApiAccToAccClose143Request instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiAccToAccClose143Request.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private ATAC143Request srvRequest;
    private com.sbt.pprb.dto.accdi.Message requestModel;
    private TransferResponse responseModel;
    private String uuid;

    private ApiAccToAccClose143Request() {
        srvRequest = createTransportProxy(ATAC143Request.class);
    }

    public static ApiAccToAccClose143Request getInstance() {
        if (instance == null) {
            instance = new ApiAccToAccClose143Request();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        File templateXml = new File("src/test/resources/xml/acc2AccClose143.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (com.sbt.pprb.dto.accdi.Message) parser.getObject(templateXml, com.sbt.pprb.dto.accdi.Message.class);

        uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar operDate = Calendar.getInstance();

        requestModel.getAccToAccClose143Request().setUser(requestModel.getAccToAccClose143Request().getUser().withUUID(uuid));
        requestModel.getAccToAccClose143Request().setUser(requestModel.getAccToAccClose143Request().getUser().withOperDate(operDate));

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        Map<String, Object> mapa = new LinkedHashMap<String, Object>();
        try {
            Request<TransferResponse> request = srvRequest.accToAccClose143(requestModel.getAccToAccClose143Request(), mapa);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            System.out.println("TransferResponse: Код ошибки = " + responseModel.getError().getMessage());
            LOG.info("TransferResponse: Текст ошибки = " + responseModel.getError().getMessage());
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getError() != null, Matchers.is(true));
        }
    }
}