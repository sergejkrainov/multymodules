package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.CommunalPaymentResponse;
import com.sbt.pprb.dto.accdi.Message;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.pprb.alltopayment.publicapi.HouseCertificateAccountToPaymentRequest;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by sbt-sharikov-pi on 17.10.2017.
 */
public class ApiHouseCertificateAccountToPaymentRequest extends ApiBaseTest {

    private static ApiHouseCertificateAccountToPaymentRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiSrvCardToPaymentRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private HouseCertificateAccountToPaymentRequest srvRequest;
    private Message requestModel;
    private CommunalPaymentResponse responseModel;
    private String uuid;

    private ApiHouseCertificateAccountToPaymentRequest() {
        srvRequest = createTransportProxy(HouseCertificateAccountToPaymentRequest.class);
    }

    public static ApiHouseCertificateAccountToPaymentRequest getInstance() {
        if (instance == null) {
            instance = new ApiHouseCertificateAccountToPaymentRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        File templateXml = new File("src/test/resources/xml/houseCertificateAccountToPaymentRequest.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (Message) parser.getObject(templateXml, Message.class);

        uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar operDate = Calendar.getInstance();

        requestModel.getAccToCommunalRequest().setUser(requestModel.getAccToCommunalRequest().getUser().withUUID(uuid));
        requestModel.getAccToCommunalRequest().setUser(requestModel.getAccToCommunalRequest().getUser().withOperDate(operDate));

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<CommunalPaymentResponse> request =
                    srvRequest.accToCommunalRequest(requestModel.getAccToCommunalRequest(), "");

            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            Assert.fail("Проблемы с выполнением запроса или чтения ответа: " + e.getMessage());
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            Message responseMessageModel = new Message().withCommunalPaymentResponse(responseModel);
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseMessageModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
            System.out.println("TransferResponse: Код ошибки = " + responseModel.getError());
            LOG.info("TransferResponse: Текст ошибки = " + responseModel.getError());
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getError() != null, Matchers.is(true));
        }
    }
}