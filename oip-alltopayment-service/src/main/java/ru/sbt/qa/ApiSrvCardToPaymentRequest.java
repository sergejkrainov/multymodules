package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.CommunalPaymentResponse;
import com.sbt.pprb.dto.accdi.Message;
import com.sbt.pprb.services.api.pprb.cardtopayment.SrvCardToPaymentRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 06.10.2017.
 */
public class ApiSrvCardToPaymentRequest extends ApiBaseTest {

    private static ApiSrvCardToPaymentRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiSrvCardToPaymentRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private SrvCardToPaymentRequest srvRequest;
    private Message requestModel;
    private CommunalPaymentResponse responseModel;
    private String uuid;

    private ApiSrvCardToPaymentRequest() {
        srvRequest = createTransportProxy(SrvCardToPaymentRequest.class);
    }

    public static ApiSrvCardToPaymentRequest getInstance() {
        if (instance == null) {
            instance = new ApiSrvCardToPaymentRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/cardToPaymentRequest.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (Message) parser.getObject(templateXml, Message.class);

        uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar operDate = Calendar.getInstance();

        requestModel.getCardToCommunalRequest().setUser(requestModel.getCardToCommunalRequest().getUser().withUUID(uuid));
        requestModel.getCardToCommunalRequest().setUser(requestModel.getCardToCommunalRequest().getUser().withOperDate(operDate));

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<CommunalPaymentResponse> request = srvRequest.cardToPayment(requestModel.getCardToCommunalRequest());
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            Message responseMessageModel = new Message().withCommunalPaymentResponse(responseModel);
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseMessageModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
            System.out.println("TransferResponse: Код ошибки = " + responseModel.getError());
            LOG.info("TransferResponse: Текст ошибки = " + responseModel.getError());
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getError() != null, Matchers.is(true));
        }
    }
}