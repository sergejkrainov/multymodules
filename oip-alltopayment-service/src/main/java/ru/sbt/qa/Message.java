package ru.sbt.qa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created by sbt-sharikov-pi on 17.10.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "Message",
        propOrder = "accToCommunalRequest"
)
@XmlRootElement(
        name = "message", namespace = "XMLDepoCOD"
)
public class Message implements Serializable{
    private static final long serialVersionUID = 1L;

    public Message() {
    }

}