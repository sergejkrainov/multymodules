package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.transfermoney.InitiateExecCashlessCommonOperationRq;
import com.sbt.pprb.dto.transfermoney.InitiateExecCashlessCommonOperationRs;
import com.sbt.pprb.services.api.pprb.execcashless.SrvInitiateExecCashlessCommonOperationRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 20.09.2017.
 */
public class ApiSrvInitiateExecCashlessCommonOperationRequest extends ApiBaseTest{

    private static ApiSrvInitiateExecCashlessCommonOperationRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiSrvInitiateExecCashlessCommonOperationRequest.class);

    private String rqUID;
    private String operUID;
    private String spName;
    private long TIMEOUT_IN_SECONDS = 60;
    private SrvInitiateExecCashlessCommonOperationRequest srvRequest;
    private InitiateExecCashlessCommonOperationRq requestModel;
    private InitiateExecCashlessCommonOperationRs responseModel;

    public ApiSrvInitiateExecCashlessCommonOperationRequest() {
        srvRequest = createTransportProxy(SrvInitiateExecCashlessCommonOperationRequest.class);
    }

    public static ApiSrvInitiateExecCashlessCommonOperationRequest getInstance() {
        if (instance == null) {
            instance = new ApiSrvInitiateExecCashlessCommonOperationRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/InitiateExecCashlessCommonOperationRq.xml");

        JaxbParser parser = new JaxbParser();
        requestModel = (InitiateExecCashlessCommonOperationRq) parser.getObject(templateXml,  InitiateExecCashlessCommonOperationRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        Calendar operationDay = Calendar.getInstance();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        requestModel.setOperationDay(operationDay);
        spName = requestModel.getSPName();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<InitiateExecCashlessCommonOperationRs> request = srvRequest.invoke(requestModel);

            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.toString());
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное сообщение xml:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + responseModel.getOperUID(),
                    operUID, Matchers.equalTo(responseModel.getOperUID()));
            ec.checkThat("SPName в исходном сообщении и в ответе не равны " + spName + "!=" + responseModel.getSystemId(),
                    spName, Matchers.equalTo(responseModel.getSystemId()));
        }

    }
}