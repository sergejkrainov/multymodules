package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.usvbo.DepositPrivateAccountRq;
import com.sbt.pprb.dto.usvbo.DepositPrivateAccountRs;
import com.sbt.pprb.services.api.pprb.depositprivateaccount.DepositPrivateAccountRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 06.10.2017.
 */
public class ApiDepositPrivateAccountRequest extends ApiBaseTest {

    private static ApiDepositPrivateAccountRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiDepositPrivateAccountRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private DepositPrivateAccountRequest srvRequest;
    private DepositPrivateAccountRq requestModel;
    private DepositPrivateAccountRs responseModel;
    private String rqUID;
    private String spName;
    private String operUID;

    private ApiDepositPrivateAccountRequest() {
        srvRequest = createTransportProxy(DepositPrivateAccountRequest.class);
    }

    public static ApiDepositPrivateAccountRequest getInstance() {
        if (instance == null) {
            instance = new ApiDepositPrivateAccountRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/DepositPrivateAccountRq.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (DepositPrivateAccountRq) parser.getObject(templateXml, DepositPrivateAccountRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        requestModel.getPaymentRequest().getPaymentDetails().setDate(rqTm);
        requestModel.getPaymentRequest().getPaymentDetails().setSourceDocDate(rqTm);
        spName = requestModel.getSPName();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<DepositPrivateAccountRs> request = srvRequest.invoke(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("SPName в исходном сообщении и в ответе не равны " + spName + "!=" + responseModel.getSystemId(),
                    spName, Matchers.equalTo(responseModel.getSystemId()));
        }
    }
}