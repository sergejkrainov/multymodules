package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.hubbleemissionprovider.InitiateChangeTariffCardRq;
import com.sbt.pprb.dto.hubbleemissionprovider.InitiateChangeTariffCardRs;
import com.sbt.pprb.services.api.pprb.initiatechangetariffcard.InitiateChangeTariffCardRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by sbt-sharikov-pi on 09.10.2017.
 */
public class InitiateChangeTariffCardApi extends ApiBaseTest{
    private static InitiateChangeTariffCardApi instance;
    private static final Logger LOG = LoggerFactory.getLogger(InitiateChangeTariffCardApi.class);

    private String rqUID;
    private String operUID;
    private long TIMEOUT_IN_SECONDS = 60;
    private long SLEEP_IN_MILLISECONDS = 120000;
    private InitiateChangeTariffCardRequest srvRequest;
    private InitiateChangeTariffCardRq requestModel;
    private InitiateChangeTariffCardRs response;

    private InitiateChangeTariffCardApi() {
        srvRequest = createTransportProxy(InitiateChangeTariffCardRequest.class);
    }

    public static InitiateChangeTariffCardApi getInstance() {
        if (instance == null) {
            instance = new InitiateChangeTariffCardApi();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        File templateXml= new File("src/test/resources/xml/InitiateChangeTariffCardRq.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (InitiateChangeTariffCardRq) parser.getObject(templateXml, InitiateChangeTariffCardRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        requestModel.setRqUID(rqUID);
        requestModel.setOperUID(operUID);

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Thread.sleep(SLEEP_IN_MILLISECONDS);
            Request<InitiateChangeTariffCardRs> request = srvRequest.invoke(requestModel);
            response = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", response != null, Matchers.is(true));
        if(response != null) {
            LOG.info("\nПараметры полученного сообщения:");
            LOG.info("getOperUID = " + response.getOperUID());
            LOG.info("getRqUID = " + response.getRqUID());
            LOG.info("getSystemId = " + response.getSystemId());
            LOG.info("getStatusCode = " + response.getStatus().getStatusCode());
            LOG.info("getStatusDesc = " + response.getStatus().getStatusDesc());

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + response.getRqUID(),
                    rqUID, Matchers.equalTo(response.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + response.getOperUID(),
                    operUID, Matchers.equalTo(response.getOperUID()));
        }
    }
}