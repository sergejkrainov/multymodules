package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.hubbleemissionprovider.InitiateRepeatableCardIssueRq;
import com.sbt.pprb.dto.hubbleemissionprovider.InitiateRepeatableCardIssueRs;
import com.sbt.pprb.services.api.pprb.initiaterepeatablecardissue.InitiateRepeatableCardIssueApiRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by sbt-sharikov-pi on 09.10.2017.
 */
public class InitiateRepeatableCardIssueApi extends ApiBaseTest{

    private static InitiateRepeatableCardIssueApi instance;
    private static final Logger LOG = LoggerFactory.getLogger(InitiateRepeatableCardIssueApi.class);

    private String rqUID;
    private String operUID;
    private String spName;
    private long TIMEOUT_IN_SECONDS = 60;
    private InitiateRepeatableCardIssueApiRequest srvRequest;
    private InitiateRepeatableCardIssueRq requestModel;
    private InitiateRepeatableCardIssueRs responseModel;

    private InitiateRepeatableCardIssueApi() {
        srvRequest = createTransportProxy(InitiateRepeatableCardIssueApiRequest.class);
    }

    public static InitiateRepeatableCardIssueApi getInstance() {
        if (instance == null) {
            instance = new InitiateRepeatableCardIssueApi();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        File templateXml= new File("src/test/resources/xml/InitiateRepeatableCardIssue.xml");
        System.out.println(templateXml.list());
        JaxbParser parser = new JaxbParser();
        requestModel = (InitiateRepeatableCardIssueRq) parser.getObject(templateXml, InitiateRepeatableCardIssueRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        spName = requestModel.getSPName();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    @Override
    public void sendRequest() throws Exception {
        try {
            Request<InitiateRepeatableCardIssueRs> request = srvRequest.invoke(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное сообщение xml:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + responseModel.getOperUID(),
                    operUID, Matchers.equalTo(responseModel.getOperUID()));
            ec.checkThat("SPName в исходном сообщении и в ответе не равны " + spName + "!=" + responseModel.getSystemId(),
                    spName, Matchers.equalTo(responseModel.getSPName()));
        }
    }
}