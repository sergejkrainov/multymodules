#language:ru
@All @smoke
Функционал: {oip-deposit-card}

  @oip-deposit-card
  @InitiateRepeatableCardIssueApiRequest
  Сценарий: {InitiateRepeatableCardIssueApiRequest}
    Дано Создан прокси транспорта для сервиса InitiateRepeatableCardIssueApi
    * Построить модель данных по xml
    * Отправить запрос
    * Проверить полученный ответ