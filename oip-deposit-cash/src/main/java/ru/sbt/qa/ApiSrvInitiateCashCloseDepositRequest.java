package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.transfermoney.InitiateCashCloseDepositRq;
import com.sbt.pprb.dto.transfermoney.InitiateCashCloseDepositRs;
import com.sbt.pprb.services.api.pprb.cashclosedeposit.SrvInitiateCashCloseDepositRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 09.10.2017.
 */
public class ApiSrvInitiateCashCloseDepositRequest extends ApiBaseTest {

    private static ApiSrvInitiateCashCloseDepositRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiSrvInitiateCashCloseDepositRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private SrvInitiateCashCloseDepositRequest srvRequest;
    private InitiateCashCloseDepositRq requestModel;
    private InitiateCashCloseDepositRs responseModel;
    private String rqUID;
    private String operUID;
    private String systemId;

    private ApiSrvInitiateCashCloseDepositRequest() {
        srvRequest = createTransportProxy(SrvInitiateCashCloseDepositRequest.class);
    }

    public static ApiSrvInitiateCashCloseDepositRequest getInstance() {
        if (instance == null) {
            instance = new ApiSrvInitiateCashCloseDepositRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/InitiateCashCloseDepositRq.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (InitiateCashCloseDepositRq) parser.getObject(templateXml, InitiateCashCloseDepositRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        systemId = requestModel.getSystemId();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<InitiateCashCloseDepositRs> request = srvRequest.invoke(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("OperUID в исходном сообщении и в ответе не равны " + operUID + "!=" + responseModel.getOperUID(),
                    operUID, Matchers.equalTo(responseModel.getOperUID()));
            ec.checkThat("SystemId в исходном сообщении и в ответе не равны " + systemId + "!=" + responseModel.getSystemId(),
                    systemId, Matchers.equalTo(responseModel.getSystemId()));

        }
    }
}