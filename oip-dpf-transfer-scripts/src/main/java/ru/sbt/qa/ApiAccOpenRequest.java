package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.Message;
import com.sbt.pprb.dto.accdi.TransferOpenResponse;
import com.sbt.pprb.services.api.pprb.tdo.AORequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 06.10.2017.
 */
public class ApiAccOpenRequest extends ApiBaseTest {

    private static ApiAccOpenRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiAccOpenRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private AORequest srvRequest;
    private Message requestModel;
    private TransferOpenResponse responseModel;
    private String uuid;

    private ApiAccOpenRequest() {
        srvRequest = createTransportProxy(AORequest.class);
    }

    public static ApiAccOpenRequest getInstance() {
        if (instance == null) {
            instance = new ApiAccOpenRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {
        File templateXml= new File("src/test/resources/xml/AccOpenRequest.xml");
        JaxbParser parser = new JaxbParser();
        //Модель AccOpenRequest обернута в Message, где присутствует @XmlRootElement, поэтому работаем с ней
        requestModel = (Message) parser.getObject(templateXml, Message.class);

        uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar operDate = Calendar.getInstance();

        requestModel.getAccOpenRequest().setUser(requestModel.getAccOpenRequest().getUser().withOperDate(operDate));
        requestModel.getAccOpenRequest().setUser(requestModel.getAccOpenRequest().getUser().withUUID(uuid));

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<TransferOpenResponse> request = srvRequest.accOpen(requestModel.getAccOpenRequest(), getClientBankId());
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("dpf-transfer-scripts").execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            Message responseMessage = new Message();
            responseMessage.setTransferOpenResponse(responseModel);
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseMessage);
            LOG.info("Полученное сообщение xml:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
            ec.checkThat("Наличие кода ответа", responseModel != null && responseModel.getError() != null, Matchers.is(true));
        }
    }


    protected Map<String, Object> getClientBankId() {
        Map<String, Object> result = new HashMap<>();
        result.put("ClientBankId", "13");
        return result;
    }
}