#language:ru
@All @smoke
Функционал: {oip-dpf-transfer-scripts}

  @oip-dpf-transfer-scripts
  @AccOpenRequest
  Сценарий: {AccOpenRequest}
    Дано Создан прокси транспорта для сервиса ApiAccOpenRequest
    * Построить модель данных по xml
    * Отправить запрос
    * Проверить полученный ответ
