package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.pprb.paymentGenerator.publicapi.dto.*;
import ru.sbt.pprb.paymentGenerator.publicapi.payment.PaymentGeneratorServcieRequest;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by out-mashnev-ma on 10.10.2017.
 */
public class ApiGeneratePaymentPayerReqRecipinetReq extends ApiBaseTest {

    private static ApiGeneratePaymentPayerReqRecipinetReq instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiGeneratePaymentPayerReqRecipinetReq.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private PaymentGeneratorServcieRequest srvRequest;
    private GenerateMessageResponse responseModel;
    private Date operationDay;
    private String payerDeptNo;
    private String payerAccount;
    private String payerName;
    private String recipientDeptNo;
    private String payerBankBic;
    private String payerBankAccount;
    private String payerBankName;
    private String recipientAccount;
    private String recipientName;
    private String recipientBankBic;
    private String recipientBankAccount;
    private String recipientBankName;
    private BigDecimal sum;
    private BigDecimal sumCredit;
    private BigDecimal sumDebit;
    private int currency;
    private String purpose;
    private DocTypeEnum docType;
    private BusinessOperationType idOperation;
    private String gbkAccountCredit;
    private String investDocNo;
    private String outerOperUID;
    private String outerSystemId;
    private String receiverInn;
    private String receiverKPP;
    private String kbk;
    private String okato;
    private String nalogBase;
    private String nalogPeriod;
    private String numberNalogDoc;
    private Date dateNalogDoc;
    private String typeNalogPayment;
    private String docCompilerState;
    private String taxPaymentId;
    private String payerKpp;
    private String payerInn;
    private String payerAddress;
    private String payerNalogId;
    private String messageType;

    private ApiGeneratePaymentPayerReqRecipinetReq() {
        srvRequest = createTransportProxy(PaymentGeneratorServcieRequest.class);
    }

    public static ApiGeneratePaymentPayerReqRecipinetReq getInstance() {
        if (instance == null) {
            instance = new ApiGeneratePaymentPayerReqRecipinetReq();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {
    
        /*operationDay = Calendar.getInstance().getTime();
        payerDeptNo = "389038";
        payerAccount = "42301810338297605218";
        payerName = "КУЗЬМИН ПЛАТОН ИВАНОВИЧ";
        payerBankName = "ПАО СБЕРБАНК, г.МОСКВА";
        recipientDeptNo = "9040";
        payerBankBic = "044525225";
        payerBankAccount = "30101810400000000225";
        recipientAccount = "42307810340287515481";
        recipientName = "САЛМАНОВА ИРИНА МИХАЙЛОВНА";
        recipientBankBic = "044525225";
        recipientBankAccount = "30101810400000000225";
        recipientBankName = "ПАО СБЕРБАНК, г.МОСКВА";
        sum = new BigDecimal(6);
        sumCredit = new BigDecimal(6);
        sumDebit = new BigDecimal(6);
        currency = 810;
        purpose = "ПЕРЕВОД СЕСТРЕ НА СБЕРЕГАТЕЛЬНУЮКНИЖКУ, С КОТОРОЙ ОНА ОПЛАЧИВАЕТ КРЕДИТ.НАЗНАЧЕНИЕ:ДЛЯ ОПЛАТЫ КРЕДИТА";
        docType = DocTypeEnum.SB001;
        idOperation = BusinessOperationType.OPERATION_07190217;
        gbkAccountCredit = "30301810438006000000";
        investDocNo = "123456";
        outerOperUID = UUID.randomUUID().toString().replace("-","");
        outerSystemId = "SEQUESTR";
        receiverInn = "7707083893";
        receiverKPP = null;
        kbk = null;
        okato = null;
        nalogBase = null;
        nalogPeriod = null;
        numberNalogDoc = null;
        dateNalogDoc = null;
        typeNalogPayment = null;
        docCompilerState = null;
        taxPaymentId = "901234";
        payerKpp = null;
        payerInn = "7707083893";
        payerAddress = "398001 ЛИПЕЦКАЯ ЛИПЕЦК ВОСЬМОЕ МАРТА 13 102";
        payerNalogId = null;
        messageType = "211";*/
        
    
        operationDay = Calendar.getInstance().getTime();
        payerDeptNo = "389038";
        payerAccount = "42601810738299999206";
        payerName = "НЕТРОГАТЬ НЕРЕЗИДЕНТА ФАДЕЕВА";
        payerBankName = "ОАО \"СБЕРБАНК РОССИИ\" Г.МОСКВА";
        recipientDeptNo = "9040";
        payerBankBic = "044525225";
        payerBankAccount = "30101810438000000225";
        recipientAccount = "40817810000000068775";
        recipientName = "УСТИНОВ ЮРИЙ ВЛАДИМИРОВИЧ";
        recipientBankBic = "044525716";
        recipientBankAccount = "30101810440000000225";
        recipientBankName = "ОАО \"СБЕРБАНК РОССИИ\" Г.МОСКВА";
        sum = new BigDecimal(77);
        sumCredit = new BigDecimal(77);
        sumDebit = new BigDecimal(77);
        currency = 810;
        purpose = "Назначение платежа123";
        docType = DocTypeEnum.SB001;
        idOperation = BusinessOperationType.OPERATION_07190217;
        gbkAccountCredit = "30301810438006000000";
        investDocNo = "123456";
        outerOperUID = UUID.randomUUID().toString().replace("-","");
        outerSystemId = "SEQUESTR";
        receiverInn = "789256330149";
        receiverKPP = "770401001";
        kbk = "18210602010021000110";
        okato = "45376000";
        nalogBase = "ТП";
        nalogPeriod = "ГД.00.2013";
        numberNalogDoc = "0";
        dateNalogDoc = new SimpleDateFormat("dd.MM.YYYY").parse("01.02.2018");
        typeNalogPayment = "0";
        docCompilerState = "1";
        taxPaymentId = "901234";
        payerKpp = "770401001";
        payerInn = "504564043471";
        payerAddress = "Адрес плательщика из тестовой джобы";
        payerNalogId = "123456789011";
        messageType = "334";

        LOG.info("Данные отправляемого сообщения:\n" +
                 "\noperationDay = " + operationDay +
                 "\npayerAccount = " + payerAccount +
                 "\npayerName = " + payerName +
                 "\npayerBankName = " + payerBankName +
                 "\npayerBankBic = " + payerBankBic +
                 "\npayerBankAccount = " + payerBankAccount +
                 "\nrecipientAccount = " + recipientAccount +
                 "\nrecipientName = " + recipientName +
                 "\nrecipientBankBic = " + recipientBankBic +
                 "\nrecipientBankAccount = " + recipientBankAccount +
                 "\nrecipientBankName = " + recipientBankName +
                 "\nsum = " + sum +
                 "\nsumCredit = " + sumCredit +
                 "\nsumDebit = " + sumDebit +
                 "\ncurrency = " + currency +
                 "\npurpose = " + purpose +
                 "\ndocType = " + docType.toString() +
                 "\nidOperation = " + idOperation.toString() +
                 "\ngbkAccountCredit = " + gbkAccountCredit +
                 "\ninvestDocNo = " + investDocNo +
                 "\nouterOperUID = " + outerOperUID +
                 "\nouterSystemId = " + outerSystemId +
                 "\nreceiverInn = " + receiverInn +
                 "\nreceiverKPP = " + receiverKPP +
                 "\nkbk = " + kbk +
                 "\nokato = " + okato +
                 "\nnalogBase = " + nalogBase +
                 "\nnalogPeriod = " + nalogPeriod +
                 "\nnumberNalogDoc = " + numberNalogDoc +
                 "\ndateNalogDoc = " + (dateNalogDoc != null ? new SimpleDateFormat("dd.MM.YYYY").format(dateNalogDoc) : "null") +
                 "\ntypeNalogPayment = " + typeNalogPayment +
                 "\ndocCompilerState = " + docCompilerState +
                 "\ntaxPaymentId = " + taxPaymentId +
                 "\npayerKpp = " + payerKpp +
                 "\npayerInn = " + payerInn +
                 "\npayerAddress = " + payerAddress +
                 "\npayerNalogId = " + payerNalogId +
                 "\nmessageType = " + messageType
        );
    }

    public void sendRequest() throws Exception {

        try {
            BankDataDto payerBankData = new BankDataDto(payerDeptNo);
            PayerDataDto payerData = new PayerDataDto(payerBankData, payerAccount, payerName);
            payerData.setKpp(payerKpp);
            payerData.setInn(payerInn);
            payerData.setAddress(payerAddress);
            payerData.setNalogId(payerNalogId);

            //BankDataDto receiverBankData = new BankDataDto(recipientBankBic, recipientBankAccount, recipientBankName);
            BankDataDto receiverBankData = new BankDataDto(recipientBankBic, recipientBankAccount, recipientBankName);
            ReceiverDataDto receiverData = new ReceiverDataDto(receiverBankData, recipientAccount, recipientName);
            receiverData.setInn(receiverInn);
            receiverData.setKpp(receiverKPP);
            NalogInfoDto nalogInfo = new NalogInfoDto(kbk, okato, nalogBase, nalogPeriod, numberNalogDoc, dateNalogDoc,
                    typeNalogPayment, docCompilerState, taxPaymentId);
            receiverData.setNalogInfo(nalogInfo);
            OuterCallDataDto outerCallData = new OuterCallDataDto(outerOperUID, outerSystemId);
            PaymentDataDto requestData = new PaymentDataDto(
                    payerData,
                    receiverData,
                    sum,
                    sumCredit,
                    sumDebit,
                    (long) currency,
                    purpose,
                    docType,
                    idOperation,
                    gbkAccountCredit,
                    investDocNo,
                    new Date(),
                    new Random().nextLong(),
                    messageType
            ).withOuterCallData(outerCallData);

            Request<GenerateMessageResponse> request = srvRequest.generate(requestData);

            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("payment-generator").execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Получен ответ:");
            LOG.info("Код ответа: " + responseModel.getStatus().getStatusCode().toString());
            LOG.info("Сообщение: " + responseModel.getStatus().getStatusDesc());
            ec.checkThat("Наличие кода ответа",
                    responseModel != null && responseModel.getStatus().getStatusCode() != null, Matchers.equalTo(true));
        }

    }
}
