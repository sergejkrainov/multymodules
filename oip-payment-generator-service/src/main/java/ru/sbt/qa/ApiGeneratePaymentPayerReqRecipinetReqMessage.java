package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.pprb.paymentGenerator.publicapi.dto.BankDataDto;
import ru.sbt.pprb.paymentGenerator.publicapi.dto.GenerateMessageResponse;
import ru.sbt.pprb.paymentGenerator.publicapi.dto.MessageDataDto;
import ru.sbt.pprb.paymentGenerator.publicapi.participant.ParticipantMessageGeneratorRequest;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by out-mashnev-ma on 10.10.2017.
 */
public class ApiGeneratePaymentPayerReqRecipinetReqMessage extends ApiBaseTest {

    private static ApiGeneratePaymentPayerReqRecipinetReqMessage instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiGeneratePaymentPayerReqRecipinetReq.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private ParticipantMessageGeneratorRequest srvRequest;
    private GenerateMessageResponse responseModel;
    private Date operationDay;
    private String payerDeptNo;
    private String recipientDeptNo;
    private String payerBankBic;
    private String payerBankAccount;
    private String payerBankName;
    private String recipientBankBic;
    private String recipientBankAccount;
    private String recipientBankName;
    private String purpose;
    private String outerSystemId;
    private String senderTerbank;

    private ApiGeneratePaymentPayerReqRecipinetReqMessage() {
        srvRequest = createTransportProxy(ParticipantMessageGeneratorRequest.class);
    }

    public static ApiGeneratePaymentPayerReqRecipinetReqMessage getInstance() {
        if (instance == null) {
            instance = new ApiGeneratePaymentPayerReqRecipinetReqMessage();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {
        operationDay = Calendar.getInstance().getTime();
        payerDeptNo = "9038";
        payerBankName = "ОАО \"СБЕРБАНК РОССИИ\" Г.МОСКВА";
        recipientDeptNo = "6901";
        payerBankBic = "044525225";
        payerBankAccount = "30101810438000000225";
        recipientBankBic = "044525225";
        recipientBankAccount = "30101810400000000225";
        recipientBankName = "ОАО \"СБЕРБАНК РОССИИ\" Г.МОСКВА";
        purpose = "Тестовый платёж";
        outerSystemId = "Тестовая джоба";
        senderTerbank = "38";
        LOG.info("Данные отправляемого сообщения:\n" +
                 "\noperationDay = " + operationDay +
                 "\npayerBankName = " + payerBankName +
                 "\npayerBankBic = " + payerBankBic +
                 "\npayerBankAccount = " + payerBankAccount +
                 "\nrecipientBankBic = " + recipientBankBic +
                 "\nrecipientBankAccount = " + recipientBankAccount +
                 "\nrecipientBankName = " + recipientBankName +
                 "\npurpose = " + purpose +
                 "\nouterSystemId = " + outerSystemId +
                 "\nsenderTerbankNo = " + senderTerbank
        );
    }

    public void sendRequest() throws Exception {

        try {
            BankDataDto payerBankData = new BankDataDto(payerDeptNo);
            BankDataDto receiverBankData = new BankDataDto(recipientDeptNo);
            MessageDataDto requestData = new MessageDataDto(
                    new Date(),
                    new Random().nextLong(),
                    "335",
                    payerBankData,
                    senderTerbank,
                    receiverBankData,
                    "Тестовое сообщение участнику");

            Request<GenerateMessageResponse> request = srvRequest.generate(requestData);

            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).onModule("payment-generator").execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Получен ответ:");
            LOG.info("Код ответа: " + responseModel.getStatus().getStatusCode().toString());
            LOG.info("Сообщение: " + responseModel.getStatus().getStatusDesc());
            ec.checkThat("Наличие кода ответа",
                    responseModel != null && responseModel.getStatus().getStatusCode() != null, Matchers.equalTo(true));
        }
    }
}