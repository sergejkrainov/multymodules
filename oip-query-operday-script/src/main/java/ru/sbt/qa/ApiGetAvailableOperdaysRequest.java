package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.getavailableoperdays.GetAvailableOperdaysRq;
import com.sbt.pprb.dto.getavailableoperdays.GetAvailableOperdaysRs;
import com.sbt.pprb.services.api.pprb.getavailableoperdays.GetAvailableOperdaysRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 09.10.2017.
 */
public class ApiGetAvailableOperdaysRequest extends ApiBaseTest {

    private static ApiGetAvailableOperdaysRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiGetAvailableOperdaysRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private GetAvailableOperdaysRequest srvRequest;
    private GetAvailableOperdaysRq requestModel;
    private GetAvailableOperdaysRs responseModel;
    private String rqUID;
    private String operUID;
    private String spName;

    private ApiGetAvailableOperdaysRequest() {
        srvRequest = createTransportProxy(GetAvailableOperdaysRequest.class);
    }

    public static ApiGetAvailableOperdaysRequest getInstance() {
        if (instance == null) {
            instance = new ApiGetAvailableOperdaysRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/GetAvailableOperdaysRq.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (GetAvailableOperdaysRq) parser.getObject(templateXml, GetAvailableOperdaysRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        spName = requestModel.getSPName();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<GetAvailableOperdaysRs> request = srvRequest.invoke(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("SPName в исходном сообщении и в ответе не равны " + spName + "!=" + responseModel.getSPName(),
                    spName, Matchers.equalTo(responseModel.getSPName()));
        }
    }
}