package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.getcurrentopenoperday.GetCurrentOpenOperdayRq;
import com.sbt.pprb.dto.getcurrentopenoperday.GetCurrentOpenOperdayRs;
import com.sbt.pprb.services.api.pprb.getcurrentopenoperday.GetCurrentOpenOperdayRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 09.10.2017.
 */
public class ApiGetCurrentOpenOperdayRequest extends ApiBaseTest {

    private static ApiGetCurrentOpenOperdayRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiGetCurrentOpenOperdayRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private GetCurrentOpenOperdayRequest srvRequest;
    private GetCurrentOpenOperdayRq requestModel;
    private GetCurrentOpenOperdayRs responseModel;
    private String rqUID;
    private String operUID;
    private String spName;

    private ApiGetCurrentOpenOperdayRequest() {
        srvRequest = createTransportProxy(GetCurrentOpenOperdayRequest.class);
    }

    public static ApiGetCurrentOpenOperdayRequest getInstance() {
        if (instance == null) {
            instance = new ApiGetCurrentOpenOperdayRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml = new File("src/test/resources/xml/GetCurrentOpenOperdayRq.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (GetCurrentOpenOperdayRq) parser.getObject(templateXml, GetCurrentOpenOperdayRq.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

        requestModel.setRqUID(rqUID);
        requestModel.setRqTm(rqTm);
        requestModel.setOperUID(operUID);
        spName = requestModel.getSPName();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<GetCurrentOpenOperdayRs> request = srvRequest.invoke(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное xml сообщение:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getRqUID()));
            ec.checkThat("SPName в исходном сообщении и в ответе не равны " + spName + "!=" + responseModel.getSystemId(),
                    spName, Matchers.equalTo(responseModel.getSystemId()));

        }
    }
}