package ru.sbt.qa;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import ru.sbt.qa.libs.Helper;
import ru.sbt.qa.libs.Props;
import ru.sbt.qa.variables.CalculatedVariables;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static ru.sbt.qa.stepDefs.AfterTest.ec;

public class IsPageOpeningPU{
    private static final Logger LOG = LoggerFactory.getLogger(IsPageOpeningPU.class);
	
	public void testIsPageOpening() throws Exception{
		Props.readProperties("src/test/resources/config/application-" + System.getProperty("occ.stand.name").toLowerCase() + ".properties");
		try {
			    sendHTTPRequest(Props.get("PUURL"));
                ec.checkThat("Ошибка сравнения Актуального и Ожидаемого значения тэгов " + CalculatedVariables.actualValues.get("responseCode") + "!=" + CalculatedVariables.expectedValues.get("responseCode"),
                        CalculatedVariables.actualValues.get("responseCode"), Matchers.equalTo(CalculatedVariables.expectedValues.get("responseCode")));

		}catch(Exception e) {
			LOG.error("Ошибка при выполнении теста: ", e);
			ec.addError(e);
        }
		
		
	}

     public void sendHTTPRequest(String url) throws Exception{

		HttpURLConnection con = null;
		int responseCode;
		try {

            LOG.info("PU!");
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			//add reuqest header
			con.setRequestMethod("GET");
			con.connect();
			responseCode = con.getResponseCode();
			LOG.info("\nSending 'GET' request to URL : " + url);
            LOG.info("Response Code : " + responseCode);
            LOG.info("ResponseMessage : " + con.getResponseMessage());

			CalculatedVariables.actualValues.put("responseCode", String.valueOf(responseCode));
			CalculatedVariables.expectedValues.put("responseCode", "200");

		}catch(Exception e) {
			LOG.error("Ошибка при отправке http запроса!", e);
			ec.addError(e);
			Assert.fail("Ошибка при отправке http запроса: " + e.getMessage());
		}finally {
			if (con != null) {
				con.disconnect();
			}
		}

        }


}
