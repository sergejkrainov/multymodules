package ru.sbt.qa.stepDefs;


import cucumber.api.java.ru.Когда;
import ru.sbt.qa.IsPageOpeningPU;

/**
 * Created by sbt-yulun-vv on 19.05.2017.
 */
public class PuStepDefs {
    @Когда("^Открыть страницу для модуля PU и выполнить сценарий IsPageOpeningPU$")
    public void RunIsPageOpeningSosReports() throws Throwable{
        IsPageOpeningPU isPageOpeningSosReports = new IsPageOpeningPU();
        isPageOpeningSosReports.testIsPageOpening();
    }
}