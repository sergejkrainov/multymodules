package ru.sbt.qa.variables;

import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Класс для хранения глобальных вычисляемых переменных
 *
 * @author sbt-kraynov-sa
 */
public class CalculatedVariables {
    public static WebDriver webDriver;

    public static LinkedHashMap<String, String> actualValues = new LinkedHashMap<String, String>();
    public static LinkedHashMap<String, String> expectedValues = new LinkedHashMap<String, String>();

    // Хранилище всего скачанного лога разбитого по строчкам, для контроля нарезки лога для текущего теста
    public static ArrayList<String> parseLog = new ArrayList<>();

    // Хранилище всего что полезно для тестов и разбияния теста на фичи.
    public static LinkedHashMap<String, Object> stash = new LinkedHashMap<>();
}