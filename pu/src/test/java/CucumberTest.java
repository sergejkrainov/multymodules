import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, plugin = {"pretty", "ru.yandex.qatools.allure.cucumberjvm.AllureReporter"},
        glue = {"ru/sbt/qa/stepDefs"},
        features = {"src/test/resources/features/"},
        tags ={"@All"}
)
public class CucumberTest {
}