package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.getunifresultstatus.*;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.regpaybatch.api.RegPayBatchServiceRequest;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbt-sharikov-pi on 24.10.2017.
 */
public class ApiNotifyUnifStatusRequest extends ApiBaseTest {

    private String rqUID;
    private String operUID;
    private String spName;
    private long TIMEOUT_IN_SECONDS = 60;

    private static ApiNotifyUnifStatusRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiNotifyUnifStatusRequest.class);
    private RegPayBatchServiceRequest srvRequest;
    private GetUNIFResultStatusRq requestModel;
    private GetUNIFResultStatusRs response;

    private ApiNotifyUnifStatusRequest() {
        srvRequest = createTransportProxy(RegPayBatchServiceRequest.class);
    }

    public static ApiNotifyUnifStatusRequest getInstance() {
        if (instance == null) {
            instance = new ApiNotifyUnifStatusRequest();
        }
        return instance;
    }

    @Override
    public void createRequestModel() throws Exception {
        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();

        requestModel = new GetUNIFResultStatusRq();

        requestModel.setRqUID(rqUID);
        requestModel.setOperUID(operUID);
        requestModel.setRqTm(rqTm);
        SystemNameType snt = SystemNameType.BP_UFO;
        requestModel.setSystemId(SystemNameType.URN_SBRFSYSTEMS_99_FPBO_AV);
        requestModel.setSPName(snt);
        requestModel.setUnifName("FV000090003333333333");
        ObjectFactory obj = new ObjectFactory();
        BankInfoType bankInfo = obj.createBankInfoType();
        bankInfo.setRegionId("5920");
        requestModel.setBankInfo(bankInfo);
        System.out.println("requestModel = " + requestModel.toString());
    }

    // FIXME: 01.02.2018 починить позже
    @Override
    public void sendRequest() throws Exception {
        try {
            Request<GetUNIFResultStatusRs> request = srvRequest.getUnifProcessingStatus(requestModel);
            response = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
            System.out.println("Response code = " + response.getStatus().getStatusCode());
            System.out.println("Response code description = " + response.getStatus().getStatusDesc());
            System.out.println("Response RqUID = " + response.getRqUID());
        } catch (Exception e) {
            LOG.info("Проблемы с выполнением запроса или чтения ответа", e.getMessage());
            LOG.info("StackTrace:\n", e);
            ec.addError(e);
        }
    }

    @Override
    public void checkResponse() throws Exception {
        ec.checkThat("Наличие ответа", response != null, Matchers.is(true));
        if(response != null) {
            ec.checkThat("Наличие кода ответа", response != null && response.getStatus() != null, Matchers.is(true));
        }
    }
}