package ru.sbt.qa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import ru.sbt.qa.libs.FileLib;
import ru.sbt.qa.libs.MQLib;
import ru.sbt.qa.libs.XMLLib;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by sbt-sharikov-pi on 13.10.2017.
 */
//TODO Вынести параметры в локальные properties
public class InitiatePaymentOrderExecRq {
    private static final Logger LOG = LoggerFactory.getLogger(InitiatePaymentOrderExecRq.class);

    public static String runDirPath = System.getProperty("user.dir");
    String readXML;
    String RqUID = UUID.randomUUID().toString().replaceAll("-", "").toLowerCase();
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String RqTm = sdf.format(cal.getTime());
    String xmlMessage;
    String receivedXML;

    public void createXML() throws Exception {
        String path = runDirPath + "/src/test/resources/xml/InitiatePaymentOrderExecRq.xml";
        readXML = FileLib.readFromFile(path, "UTF-8");
        Document xmlDoc = XMLLib.convertStringToDom(readXML, "UTF-8");
        xmlDoc = XMLLib.setXMLTagValue(xmlDoc, "RqUID", RqUID);
        xmlDoc = XMLLib.setXMLTagValue(xmlDoc, "RqTm", RqTm);
        xmlMessage = XMLLib.createXMLMessage(xmlDoc);
        MQLib.clearQueue("ESB.PPRB.IM.RESPONSE", "sbt-oabs-361.ca.sbrf.ru", "1414",
                "PPRB.QM", "PPRB.TEST.SVRCONN");
    }

    public void sendMsgToMQ() throws Exception {
        MQLib.sendMessageToMQ(xmlMessage, "ESB.PPRB.IM.REQUEST",
                "sbt-oabs-361.ca.sbrf.ru", "1414", "PPRB.QM", "PPRB.TEST.SVRCONN");
    }

    public void getMsgFromMQ() throws Exception {
        receivedXML = MQLib.receiveMessageFromMQ("ESB.PPRB.IM.RESPONSE", "sbt-oabs-361.ca.sbrf.ru", "1414",
                "PPRB.QM", "PPRB.TEST.SVRCONN", "SrvInitiatePaymentOrderExec");
        LOG.info("\n Полученное сообщение:");
        LOG.info("\n" + readXML);
    }
}