package ru.sbt.qa.stepDefs;


import cucumber.api.java.ru.Когда;
import ru.sbt.qa.InitiatePaymentOrderExecRq;

/**
 * Created by sbt-yulun-vv on 19.05.2017.
 */
public class RegpayBatchStepDefs {
    @Когда("^Открыть страницу для модуля sos-reports и выполнить сценарий InitiatePaymentOrderExecRq$")
    public void RunIsPageOpeningSosReports() throws Throwable{
        InitiatePaymentOrderExecRq initiatePaymentOrderExecRq = new InitiatePaymentOrderExecRq();
        initiatePaymentOrderExecRq.createXML();
        initiatePaymentOrderExecRq.sendMsgToMQ();
        initiatePaymentOrderExecRq.getMsgFromMQ();
    }
}