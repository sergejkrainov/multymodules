package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.sos.deposit.deny.api.CheckServiceAccountRequest;
import com.sbt.pprb.sos.deposit.deny.api.model.IsAttendRequest;
import com.sbt.pprb.sos.deposit.deny.api.model.IsAttendResponse;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by out-mashnev-ma on 10.10.2017.
 */
public class ApiCheckServiceAccountRequest extends ApiBaseTest {

    private static ApiCheckServiceAccountRequest instance;
    private static final Logger LOG = LoggerFactory.getLogger(ApiCheckServiceAccountRequest.class);

    private long TIMEOUT_IN_SECONDS = 60;
    private CheckServiceAccountRequest srvRequest;
    private IsAttendRequest requestModel;
    private IsAttendResponse responseModel;

    private ApiCheckServiceAccountRequest() {
        srvRequest = createTransportProxy(CheckServiceAccountRequest.class);
    }

    public static ApiCheckServiceAccountRequest getInstance() {
        if (instance == null) {
            instance = new ApiCheckServiceAccountRequest();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        requestModel = new IsAttendRequest();
        requestModel.setBranch(8641);
        requestModel.setFosb(166);


       LOG.info("Сформатированная xml модель для отправки:\n" +
               "\nIsAttendRequest.getBranch = " + requestModel.getBranch() +
               "\nIsAttendRequest.getFosb = " + requestModel.getFosb());
    }

    public void sendRequest() throws Exception {

        try {
            Request<IsAttendResponse> request = srvRequest.isAttend(requestModel);
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа: ", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            LOG.info("Получен ответ:");
            LOG.info("Код ответа: " + responseModel.getRetcode());
            LOG.info("Сообщение: " + responseModel.getMessage());
            ec.checkThat("Наличие ответа",
                    responseModel != null, Matchers.equalTo(true));
        }
    }
}