package ru.sbt.qa;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.BrowserLib;
import ru.sbt.qa.libs.Props;
import ru.sbt.qa.variables.CalculatedVariables;

/**
 * Created by sbt-sharikov-pi on 12.10.2017.
 */
public class IsPageOpeningSosReports {
    private static final Logger LOG = LoggerFactory.getLogger(IsPageOpeningSosReports.class);

    public void testIsPageOpening() throws Exception {
        Props.readProperties("src/test/resources/config/application-" + System.getProperty("occ.stand.name").toLowerCase() + ".properties");
        try{

            BrowserLib.startScreenManager();
            BrowserLib.initBrowser();
            LOG.info("TagConfigFile=" + System.getProperty("user.dir") + System.getProperty("TagConfigFile"));
            LOG.info(Props.get("sosReportsURL"));
            BrowserLib.openWebPage(CalculatedVariables.webDriver, Props.get("sosReportsURL"));
            checkFields();
            BrowserLib.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
        }catch(Exception e){
            LOG.error("Ошибка при выполнении теста: ", e);
            Assert.fail(e.getMessage());
        }finally {
            BrowserLib.stopScreenManager();
        }
    }

    private void checkFields() throws Exception{
        boolean isFieldPresent = false;

        isFieldPresent = CalculatedVariables.webDriver.findElements(By.xpath("//span[text()='Отчет об открытых и закрытых счетах']")).size() > 0;
        CalculatedVariables.actualValues.put("Отчет об открытых и закрытых счетах", String.valueOf(isFieldPresent));
        CalculatedVariables.expectedValues.put("Отчет об открытых и закрытых счетах", "true");
        isFieldPresent = CalculatedVariables.webDriver.findElements(By.xpath("//span[text()='Отчет об неисполненных плановых сервисах']")).size() > 0;
        CalculatedVariables.actualValues.put("Отчет об неисполненных плановых сервисах", String.valueOf(isFieldPresent));
        CalculatedVariables.expectedValues.put("Отчет об неисполненных плановых сервисах", "true");
        CalculatedVariables.webDriver.quit();
    }


}