package ru.sbt.qa.stepDefs;


import cucumber.api.java.ru.Когда;
import ru.sbt.qa.IsPageOpeningSosReports;

/**
 * Created by sbt-yulun-vv on 19.05.2017.
 */
public class SosReportStepDefs {
    @Когда("^Открыть страницу для модуля sos-reports и выполнить сценарий IsPageOpeningSosReports$")
    public void RunIsPageOpeningSosReports() throws Throwable {
        IsPageOpeningSosReports isPageOpeningSosReports = new IsPageOpeningSosReports();
        isPageOpeningSosReports.testIsPageOpening();
    }
}