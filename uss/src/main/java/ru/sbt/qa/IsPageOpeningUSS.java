package ru.sbt.qa;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.Helper;
import ru.sbt.qa.libs.Props;
import ru.sbt.qa.variables.CalculatedVariables;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class IsPageOpeningUSS {
    private static final Logger LOG = LoggerFactory.getLogger(IsPageOpeningUSS.class);
	
	public void testIsPageOpening() throws Exception{
		Props.readProperties("src/test/resources/config/application-" + System.getProperty("occ.stand.name").toLowerCase() + ".properties");
		try {
			if(System.getProperty("occ.stand.name").toLowerCase().equals("ift") ||
					System.getProperty("occ.stand.name").toLowerCase().equals("msv")) {
				Helper.startScreenManager();

				Helper.initBrowser();
	    		
	            Helper.openWebPage(CalculatedVariables.webDriver, Props.get("USSURL"));
	    		
	    		loginToUSS();
	    		
	    		checkFields();
	            
			}else {
				sendHTTPRequest(Props.get("USSURL"));
			}

	        Helper.checkHashMaps(CalculatedVariables.expectedValues, CalculatedVariables.actualValues);
		}catch(Exception e) {
			LOG.error("Ошибка при выполнении теста: ", e);
			Assert.fail();
		}finally{
            Helper.stopScreenManager();
		}
		
		
	}

	/**
	 * Функция авторизации
	 */
	public void loginToUSS() throws Exception{
		WebDriver driver = CalculatedVariables.webDriver;
		if(System.getProperty("occ.stand.name").toLowerCase().equals("ift") ||
				System.getProperty("occ.stand.name").toLowerCase().equals("msv")	){
			int countElements = driver.findElements(By.xpath("//a[@id='overridelink']")).size();
			//Соглашаемся с сертификатом
			if(countElements > 0) {
				driver.findElement(By.xpath("//a[@id='overridelink']")).click();
				Thread.sleep(2000);
			}
		}

		//Очищаем поле Домен
		driver.findElement(By.xpath("//input[@id='domain']")).clear();

		//Очищаем поле Имя пользователя
		driver.findElement(By.xpath("//input[@id='username']")).clear();
		//Заполняем поле Имя пользователя
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(Props.get("ussWebLogin"));

		//Заполняем поле Пароль
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(Props.get("ussWebPassword"));

		//Нажимаем кнопку Вход
		driver.findElement(By.xpath("//input[@id='submitInter']")).click();

		//Ожидаем авторизации 1 секунды
		Thread.sleep(200);

	}
	
	private void checkFields() throws Exception {
		WebDriver driver = CalculatedVariables.webDriver;
		boolean isFieldPresent = false;

		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Не определен/отсутствует']")).size() > 0;
		CalculatedVariables.actualValues.put("Не определен/отсутствует", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Не определен/отсутствует", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Дебетовая банковская карта']")).size() > 0;
		CalculatedVariables.actualValues.put("Дебетовая банковская карта", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Дебетовая банковская карта", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Бессрочный счет']")).size() > 0;
		CalculatedVariables.actualValues.put("Бессрочный счет", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Бессрочный счет", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Вклад']")).size() > 0;
		CalculatedVariables.actualValues.put("Вклад", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Вклад", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='ОМС']")).size() > 0;
		CalculatedVariables.actualValues.put("ОМС", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("ОМС", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Счет жилищных и ипотечных программ']")).size() > 0;
		CalculatedVariables.actualValues.put("Счет жилищных и ипотечных программ", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Счет жилищных и ипотечных программ", "true");
		
		isFieldPresent = driver.findElements(By.xpath("//span[@class='text' and text()='Универсальный']")).size() > 0;
		CalculatedVariables.actualValues.put("Универсальный", String.valueOf(isFieldPresent));								
		CalculatedVariables.expectedValues.put("Универсальный", "true");
		
		driver.quit();
	}

	public void sendHTTPRequest(String url) throws Exception{
		try {

            LOG.info("USS!");
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
			con.setRequestProperty("Accept-Charset", "UTF-8");

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			int responseCode = con.getResponseCode();
			LOG.info("\nSending 'GET' request to URL : " + url);
            LOG.info("Response Code : " + responseCode);
            LOG.info("ResponseMessage : " + con.getResponseMessage());

			CalculatedVariables.actualValues.put("responseCode", String.valueOf(responseCode));
			CalculatedVariables.expectedValues.put("responseCode", "200");



		}catch(Exception e) {
			LOG.error("Ошибка при отправке http запроса!", e);
			Assert.fail("Ошибка при отправке http запроса: " + e.getMessage());
		}
	}

}
