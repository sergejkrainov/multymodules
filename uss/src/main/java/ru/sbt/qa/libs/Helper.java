package ru.sbt.qa.libs;

import org.junit.Assert;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.variables.CalculatedVariables;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by SBT-Kraynov-SA on 19.09.2017. (Edited sbt-sharikov-pi)
 */
public class Helper {

    private static final Logger LOG = LoggerFactory.getLogger(Helper.class);
    public static WebDriver webDriver;

    /**
     * Функция инициализации драйвера для работы с браузером
     */
    public static void initBrowser() throws Exception{
        String browserName = "ie";
        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\test\\resources\\webdrivers\\IEDriverServer.exe");
        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        caps.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        caps.setCapability("ignoreProtectedModeSettings", true);
        caps.setCapability("ignoreZoomSetting", true);
        caps.setCapability("nativeEvents", false);
        caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        CalculatedVariables.webDriver = new InternetExplorerDriver(caps);
        CalculatedVariables.webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    /**
     * Функция открытия страницы
     *
     * @throws InterruptedException
     */
    public static void openWebPage(WebDriver driver, String url) throws InterruptedException {
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();
        Thread.sleep(3000);
    }

    public static void startScreenManager() throws Exception{
        String url = Props.get("screenManagerPath") + "api/screens/" + Props.get("screenManagerScreenId") + "/start";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
        String urlParameters = "{\"password\":\"" + Props.get("screenManagerPassword") + "\"}";
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        LOG.info("\nSending 'POST' request to URL : " + url);
        LOG.info("Post parameters : " + urlParameters);
        LOG.info("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        LOG.info(response.toString());
    }

    public static void stopScreenManager() throws Exception {
        String url = Props.get("screenManagerPath") + "api/screens/" + Props.get("screenManagerScreenId") + "/stop";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
        String urlParameters = "{\"password\":\"" + Props.get("screenManagerPassword") + "\"}";
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        LOG.info("\nSending 'POST' request to URL : " + url);
        LOG.info("Post parameters : " + urlParameters);
        LOG.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        LOG.info(response.toString());
    }

    /**
     * Функция проверки на равенство значений элементов в двух HashMap
     *
     * @param expectedMap - ожидаемые результаты тестов
     * @param actualMap   - текущие результаты тестов
     * @throws Exception
     */
    public static void checkHashMaps(LinkedHashMap<String, String> expectedMap, LinkedHashMap<String, String> actualMap) throws Exception {
        Set<Map.Entry<String, String>> entrSet = expectedMap.entrySet();
        Iterator<Map.Entry<String, String>> it = entrSet.iterator();
        Map.Entry<String, String> entr;
        while (it.hasNext()) {
            entr = it.next();
            LOG.info("ПРОВЕРКА ТЭГОВ " + entr.getKey() + ": " + entr.getValue() + " " + actualMap.get(entr.getKey()));
            assertEqualsValues(entr.getValue(), actualMap.get(entr.getKey()), entr.getKey());
        }
    }

    /**
     * Функция проверки двух значений на равенство
     *
     * @param actualFieldName    - название проверяемого поля
     * @param actualFieldValue   - значение проверяемого поля
     * @param expectedFieldValue - ожидаемое значение проверяемого поля
     * @throws Exception
     */
    public static <T> void assertEqualsValues(T expectedFieldValue, T actualFieldValue, String actualFieldName) throws Exception {
//        boolean isFailed = true;
        try {
            if (!expectedFieldValue.equals(actualFieldValue)) {
                LOG.error("Ошибка сравнения Актуального и Ожидаемого значения тэгов");
                Assert.fail("Ошибка сравнения: " +
                        "ожидаемого(expected) значения: " + expectedFieldValue + " и проверяемого(actual): " + actualFieldValue +
                        " в проверяемом поле " + actualFieldName);
            }
        }catch(Exception e){
            LOG.error("Ошибка сравнения двух значений HashMap", e);
            throw e;
        }
    }
}