package ru.sbt.qa.stepDefsUSS;

import cucumber.api.java.Before;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.AssumptionViolatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BeforeTest {

    private static final Logger LOG = LoggerFactory.getLogger(BeforeTest.class);

    @Before
    public void setUp() throws IOException, AssumptionViolatedException {
        PropertyConfigurator.configure("../configs/log4j.properties");
        try {
            LOG.info("Инициализируем параметры запуска теста");
            Properties properties = new Properties();
            try (InputStream streamFromResources = new FileInputStream("../configs/application.properties")) {
                properties.load(streamFromResources);
            } catch (IOException | NullPointerException e) {
                throw new RuntimeException("Failed to access properties file", e);
            }

            System.setProperty("skipAllTest", System.getProperty("skipAllTest", properties.getProperty("skipAllTest", "false")));
            String skipAllTest = (System.getProperty("skipAllTest"));
            if (skipAllTest != null && !skipAllTest.isEmpty()) {
                Assume.assumeFalse(Boolean.parseBoolean(skipAllTest));
            }

            System.setProperty("occ.stand.name", System.getProperty("occ.stand.name", properties.getProperty("occ.stand.name")));
            String stand = (System.getProperty("occ.stand.name"));

            LOG.info("occ.stand.name = " + stand);

        } catch (AssumptionViolatedException assue) {
            LOG.info("Пропускаем ошибку, если стоит skipAllTest = true!\n", assue);
            throw assue;
        } catch (Exception e) {
            LOG.info("Не удалось преднастроить запуск тестов", e);
            Assert.fail("Не удалось преднастроить запуск тестов");
        }
    }
}
