package ru.sbt.qa.stepDefsUSS;


import cucumber.api.java.ru.Когда;
import ru.sbt.qa.IsPageOpeningUSS;

/**
 * Created by sbt-yulun-vv on 19.05.2017.
 */
public class UssStepDefs {

    @Когда("^Открыть страницу для модуля USS и выполнить сценарий IsPageOpeningUSS$")
    public void RunIsPageOpeningSosReports() throws Throwable{
        IsPageOpeningUSS isPageOpeningSosReports = new IsPageOpeningUSS();
        isPageOpeningSosReports.testIsPageOpening();
    }
}