package ru.sbt.qa;

import com.sbt.core.amqp.interfaces.Request;
import com.sbt.pprb.dto.accdi.Message;
import com.sbt.pprb.dto.accdi.UpdateCoinBoxSettingsResponse;
import com.sbt.pprb.dto.accdi.UserData;
import com.sbt.pprb.services.api.pprb.SrvUpdateCoinBoxSettingsRequest;
import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.sbt.qa.libs.JaxbParser;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static ru.sbt.qa.libs.XMLLib.formatPrettyXML;

/**
 * Created by out-mashnev-ma on 27.09.2017.
 */
public class SrvUpdateCoinBoxSettingsReq extends ApiBaseTest{

    private static SrvUpdateCoinBoxSettingsReq instance;
    private static final Logger LOG = LoggerFactory.getLogger(SrvUpdateCoinBoxSettingsReq.class);

    private String rqUID;
    private String operUID;
    private String subSystem;
    private long TIMEOUT_IN_SECONDS = 60;
    private SrvUpdateCoinBoxSettingsRequest srvRequest;
    private UpdateCoinBoxSettingsResponse responseModel;
    private Message requestModel;

    private SrvUpdateCoinBoxSettingsReq() {
        srvRequest = createTransportProxy(SrvUpdateCoinBoxSettingsRequest.class);
    }

    public static SrvUpdateCoinBoxSettingsReq getInstance() {
        if (instance == null) {
            instance = new SrvUpdateCoinBoxSettingsReq();
        }
        return instance;
    }

    public void createRequestModel() throws Exception {

        File templateXml= new File("src/test/resources/xml/updateCoinBoxSettingsRequest.xml");
        JaxbParser parser = new JaxbParser();
        requestModel = (Message) parser.getObject(templateXml, Message.class);

        rqUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        operUID = java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        Calendar rqTm = Calendar.getInstance();
        Calendar operationDay = Calendar.getInstance();

        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        UserData usr = new UserData();
        usr.setRqUID(rqUID);
        usr.setUUID(operUID);
        usr.setTime(xmlGregorianCalendar);
        usr.setOperDate(operationDay);
        requestModel.getUpdateCoinBoxSettingsRequest().setUser(usr);

        subSystem = requestModel.getUpdateCoinBoxSettingsRequest().getUser().getSubSystem();

        StringWriter sw = new StringWriter();
        parser.saveObject(sw, requestModel);
        LOG.info("Сформатированная xml модель для отправки:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));
    }

    public void sendRequest() throws Exception {

        try {
            Request<UpdateCoinBoxSettingsResponse> request = srvRequest.invoke(requestModel.getUpdateCoinBoxSettingsRequest());
            responseModel = request
                    .withTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS).execute();
        } catch (Exception e) {
            LOG.error("Проблемы с выполнением запроса или чтения ответа", e);
            ec.addError(e);
        }
    }

    public void checkResponse() throws Exception {

        ec.checkThat("Наличие ответа", responseModel != null, Matchers.is(true));
        if(responseModel != null) {
            StringWriter sw = new StringWriter();
            new JaxbParser().saveObject(sw, responseModel);
            LOG.info("Полученное сообщение xml:\n\n" + formatPrettyXML(sw.toString(), "UTF-8"));

            ec.checkThat("RqUID в исходном сообщении и в ответе не равны " + rqUID + "!=" + responseModel.getError().getRqUID(),
                    rqUID, Matchers.equalTo(responseModel.getError().getRqUID()));
            ec.checkThat("SubSystem в исходном сообщении и в ответе не равны " + subSystem + "!=" + responseModel.getError().getSubSystem(),
                    subSystem, Matchers.equalTo(responseModel.getError().getSubSystem()));
        }

    }
}